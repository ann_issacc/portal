<nav class="navbar navbar-default" role="navigation">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      
      <a class="navbar-brand">Super Admin "  <?php echo $this->session->userdata('username'); ?>! "</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="active"><?php echo anchor('super/superblog', 'Home'); ?></li>
        <li><?php echo anchor('super/report', 'Reports'); ?></li>
        <li><?php echo anchor('super/register', 'Register'); ?> </li>
      </ul>
      
      <ul class="nav navbar-nav navbar-right">
        <li><h5><strong><?php echo anchor('agent/logout', 'Logout'); ?></strong></h5>
        </li>
       
            
      </ul>
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>

