<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Team_model extends CI_Model {
 
  
 
    public function __construct()
    {
        parent::__construct();
    }
	function tlogin($username,$password)
 {
  $this->db->where("username",$username);
  $this->db->where("password",$password);

  $query=$this->db->get("teamlogin");
  if($query->num_rows()>0)
  {
   foreach($query->result() as $rows)
   {
    //add all data to session
    $newdata = array(
      
      'usname'  => $rows->username,
	  
      
      'logged_in'  => TRUE,
    );
   }
   $this->session->set_userdata($newdata);
   return true;
  }
  return false;
 }
 
	public function get_verify_users($per_pg,$offset)

{
$this->db->order_by('id','desc');
$this->db->where('status', 'call back done'); 
$query = $this->db->get('agent_details',$per_pg,$offset);

return $query->result();

}
	
	
	public function get_all_users($per_pg,$offset)

{
$this->db->order_by('id','desc');
$this->db->where('status', 'not checked'); 
$query = $this->db->get('agent_details',$per_pg,$offset);

return $query->result();

}

public function insert_users_to_db($data)

{

return $this->db->insert('agent_details', $data);

}

public function getById($id){
   $query = $this->db->get_where('agent_details',array('id'=>$id));
   
   return $query->row_array();		  
 }

public function update_info($data,$id)

{

$this->db->where('agent_details.id',$id);

return $this->db->update('agent_details', $data);

}
public function message_count()
	{
		return $this->db->count_all('agent_details');
	}
	public function get_call_users($per_pg,$offset)

{
$this->db->order_by('id','desc');
$this->db->where('status', 'cancelled'); 
$query = $this->db->get('agent_details',$per_pg,$offset);

return $query->result();

}
public function get_newsale_users($per_pg,$offset)

{
$this->db->order_by('id','desc');
$this->db->where('status', 'New Lead'); 
$query = $this->db->get('agent_details',$per_pg,$offset);

return $query->result();

}
public function get_approve_users($per_pg,$offset)

{
$this->db->order_by('id','desc');
$this->db->where('status', 'approved'); 
$query = $this->db->get('agent_details',$per_pg,$offset);

return $query->result();

}
public function get_disapprove_users($per_pg,$offset)

{
$this->db->order_by('id','desc');
$this->db->where('status', 'disapproved'); 
$query = $this->db->get('agent_details',$per_pg,$offset);

return $query->result();

}
public function add_user()
 {
  $data=array(
    'timeofcall'=>$this->input->post('timeofcall'),
    'title'=>$this->input->post('title'),
    'firstname'=>$this->input->post('firstname'),
	'lastname'=>$this->input->post('lastname'),
	'saledate'=>date('Y-m-d', strtotime($this->input->post('saledate'))),
    'phonenumber'=>$this->input->post('phonenumber'),
    'address1'=>($this->input->post('address1')),
	'address2'=>($this->input->post('address2')),
	'address3'=>($this->input->post('address3')),
	'postcode'=>($this->input->post('postcode')),
	'county'=>($this->input->post('county')),
	'town'=>($this->input->post('town')),
	'fstpaydate'=>($this->input->post('fstpaydate')),
	'c_acc'=>($this->input->post('c_accno')),
	'sc_exp'=>($this->input->post('sc_expno')),
	'bankname'=>($this->input->post('bankname')),
	'bankaccountname'=>($this->input->post('bankaccountname')),
	'boxtype'=>($this->input->post('boxtype')),
	'frequency'=>($this->input->post('frequency')),
	'amount'=>($this->input->post('amount')),
	'typeoftv'=>($this->input->post('typeoftv')),
	'home_emergency'=>($this->input->post('home_emergency')),
	'password'=>($this->input->post('password')),
	'comments'=>($this->input->post('comments')),
	'centre'=>($this->input->post('centre')),
	'wgoods'=>($this->input->post('wgoods')),
	);
  $this->db->insert('agent_details',$data);
 }
	
}