<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Vcquality_model extends CI_Model {
 
  
 
    public function __construct()
    {
        parent::__construct();
    }
	function vcqualitylogin($username,$password)
 {
  $this->db->where("username",$username);
  $this->db->where("password",$password);

  $query=$this->db->get("vcqlogin");
  if($query->num_rows()>0)
  {
   foreach($query->result() as $rows)
   {
    //add all data to session
    $newdata = array(
      
      'usname'  => $rows->username,
	  
      
      'logged_in'  => TRUE,
    );
   }
   $this->session->set_userdata($newdata);
   return true;
  }
  return false;
 }
 
	
	
	
	public function get_all_users($per_pg,$offset)

{
$this->db->order_by('id','desc');
$this->db->where('status', 'sale');
$this->db->or_where('status', 'sale and upsold'); 
$query = $this->db->get('vc_customers',$per_pg,$offset);

return $query->result();

}

public function insert_users_to_db($data)

{

return $this->db->insert('vc_customers', $data);

}

public function getById($id){
   $query = $this->db->get_where('vc_customers',array('id'=>$id));
   
   return $query->row_array();		  
 }

public function update_info($data,$id)

{

$this->db->where('vc_customers.id',$id);

return $this->db->update('vc_customers', $data);

}
public function message_count()
	{
		return $this->db->count_all('vc_customers');
	}
	public function get_call_users($per_pg,$offset)

{
$this->db->order_by('id','desc');
$this->db->where('status', 'cancelled'); 
$query = $this->db->get('vc_customers',$per_pg,$offset);

return $query->result();

}
public function get_approve_users($per_pg,$offset)

{
$this->db->order_by('id','desc');
$this->db->where('status', 'approved'); 
$query = $this->db->get('vc_customers',$per_pg,$offset);

return $query->result();

}
public function get_verify_users($per_pg,$offset)

{
$this->db->order_by('id','desc');
$this->db->where('status', 'Verified Sale');
$this->db->or_where('status', 'Verified Upsold');  
$query = $this->db->get('vc_customers',$per_pg,$offset);

return $query->result();

}
public function get_disapprove_users($per_pg,$offset)

{
$this->db->order_by('id','desc');
$this->db->where('status', 'disapproved'); 
$query = $this->db->get('vc_customers',$per_pg,$offset);

return $query->result();

}

}