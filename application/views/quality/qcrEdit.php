<!DOCTYPE html>
<html lang="en">
<head>
    
</head><!--/head-->

<body id="home" class="homepage">

   
	
	
	 
</body>
</html>
<style type="text/css">
<!--
.style2 {
	font-size: x-large;
	font-weight: bold; 
}
.style3 {font-weight: bold}
-->
</style>



<?php
extract($quality);
?>
<form class="form-horizontal" method="post" action="<?php echo base_url();?>index.php/quality/qcrupdate">
<section id="work-process">
        <div class="container">
            <div class="section-header">
                <h3 class="section-title text-center wow fadeInDown" style="color:#CC0033"><strong>Welcome To QCR</strong></h3>
                <p class="text-center wow fadeInDown">
            </div>
		</p>
		<p></p>
<div class="row text-center">
<div class="col-md-2 col-md-4 col-xs-6">
                    <div class="wow fadeInUp" data-wow-duration="400ms" data-wow-delay="0ms">
                        <div class="icon-circle">
                            <span></span>
                            <i class="fa fa-history fa-2x"></i>
                        </div>
                        
                           
                    </div>
                </div>
	      <div class="col-md-6 col-md-12 col-xs-6">

   <div class="form-group">
      <label class="col-sm-2 control-label"></label>
      <div class="col-sm-10">
         
      </div>
   </div>
   <div class="form-group">
      <label for="inputPassword" class="col-sm-4 control-label">
         ID
      </label>
      <div class="col-sm-8">
         <input class="form-control" type="text" id="id" name="id"  value="<?php echo $id; ?>" >
      </div>
   </div>
  
      <div class="form-group">
         <label for="disabledTextInput"  class="col-sm-4 control-label">
            Title
         </label>
         <div class="col-sm-8">
            <input  class="form-control" type="text" id="title" name="title"  value="<?php echo $title; ?>">
         </div>
      </div>
      <div class="form-group">
         <label for="disabledSelect"  class="col-sm-4 control-label">
            First Name
         </label>
         <div class="col-sm-8">
            <input type="text" id="firstname" name="firstname"  value="<?php echo $firstname; ?>" class="form-control">
               
         </div>
      </div>
   <div class="form-group">
         <label for="disabledSelect"  class="col-sm-4 control-label">
            Last Name
         </label>
         <div class="col-sm-8">
            <input type="text" id="lastname" name="lastname"  value="<?php echo $lastname; ?>" class="form-control">
               
         </div>
      </div>
	  <div class="form-group">
         <label for="disabledSelect"  class="col-sm-4 control-label">
            Phone Number
         </label>
         <div class="col-sm-8">
            <input type="text"  id="phonenumber" name="phonenumber"  value="<?php echo $phonenumber; ?>" class="form-control">
               
         </div>
      </div>
	  <div class="form-group">
         <label for="disabledSelect"  class="col-sm-4 control-label">
            QC Agent Name
         </label>
         <div class="col-sm-8">
            <input type="text"  type="text" id="qcagentname" name="qcagentname"  value="<?php echo $this->session->userdata('usname'); ?>" class="form-control">
               
         </div>
      </div>
	  <div class="form-group">
         <label for="disabledSelect"  class="col-sm-4 control-label">
            Monitoring Date
         </label>
         <div class="col-sm-8">
            
                <input type="text"  id="date" name="monitoringdate"  value="<?php echo date("m/d/Y") ?>" class="form-control">
         </div>
      </div>
	 
	  
	  <div class="form-group">
         <label for="disabledSelect"  class="col-sm-4 control-label">
            
         </label>
         <div class="col-sm-8">
           
               
         </div>
      </div>
	   <div class="form-group">
         <label for="disabledSelect"  class="col-sm-15 control-label">
		 
            <h4 class="section-title text-center wow fadeInDown" style="color:#CC0033"><strong>Parameters</strong></h4>
         </label>
         <div class="col-sm-2">
           
               
         </div>
      </div>
	  <div class="form-group">
         <label for="disabledSelect"  class="col-sm-15 control-label">
		 
            <h5 class="section-title text-center wow fadeInDown" style="color:#990066"><strong>Prequalification</strong></h5>
         </label>
         <div class="col-sm-2">
           
               
         </div>
      </div>
   <div class="form-group has-success">
      <label class="col-sm-10 control-label" for="inputSuccess">
         Mentioning Company Name
      </label>
      <div class="col-sm-2">
         <select type="text" class="form-control" name="mentioningcompanyname" id="mentioningcompanyname">
		 <option>0</option>
	  <option>1</option>
	  <option>2</option>
	  <option>3</option>
	  <option>4</option>
	  <option>5</option>
	  </select>
      </div>
   </div>
   <div class="form-group has-success">
      <label class="col-sm-10 control-label" for="inputSuccess">
         Firm confirmation from Prospect regarding existing cover
      </label>
      <div class="col-sm-2">
         <select type="text" class="form-control" name="firmconfirmationfromprospectregardingexistingcover" id="firmconfirmationfromprospectregardingexistingcover">
		 <option>0</option>
	  <option>1</option>
	  <option>2</option>
	  <option>3</option>
	  <option>4</option>
	  <option>5</option>
	  </select>
      </div>
   </div>
   <div class="form-group has-success">
      <label class="col-sm-10 control-label" for="inputError">
         Age & Type of Equipment
      </label>
      <div class="col-sm-2">
         <select type="text" class="form-control" name="ageandtypeofequipment" id="ageandtypeofequipment" width="10">
		 <option>0</option>
	  <option>1</option>
	  <option>2</option>
	  <option>3</option>
	  <option>4</option>
	  <option>5</option>
	  </select>
      </div>
   </div>
   
   <div class="form-group has-success">
      <label class="col-sm-10 control-label" for="inputSuccess">
         Proper reason of the call mentioned along with Company Name
      </label>
      <div class="col-sm-2">
         <select type="text" class="form-control" name="properreasonofthecallmentionedalongwithcompanyname" id="properreasonofthecallmentionedalongwithcompanyname">
		 <option>0</option>
	  <option>1</option>
	  <option>2</option>
	  <option>3</option>
	  <option>4</option>
	  <option>5</option>
	  </select>
      </div>
   </div>
   <div class="form-group has-success">
      <label class="col-sm-10 control-label" for="inputWarning">
        Response to Queries of prospect (Compliant Rebuttal Used)
      </label>
      <div class="col-sm-2">
         <select type="text" class="form-control" name="Properresponsetoqueriesofprospectcompliantrebuttalusedone" id="Properresponsetoqueriesofprospectcompliantrebuttalusedone">
		 <option>0</option>
	  <option>1</option>
	  <option>2</option>
	  <option>3</option>
	  <option>4</option>
	  <option>5</option>
	  </select>
      </div>
   </div>
   <div class="form-group">
         <label for="disabledSelect"  class="col-sm-15 control-label">
		 
            <h5 class="section-title text-center wow fadeInDown" style="color:#990066"><strong>Collecting Banking Details</strong></h5>
         </label>
         <div class="col-sm-2">
           
               
         </div>
      </div>
   <div class="form-group has-success">
      <label class="col-sm-10 control-label" for="inputError">
         No Pressurizing /Badgering
      </label>
      <div class="col-sm-2">
         <select type="text" class="form-control" name="nopressurizingbadgering" id="nopressurizingbadgering" width="10">
		 <option>0</option>
	  <option>1</option>
	  <option>2</option>
	  <option>3</option>
	  <option>4</option>
	  <option>5</option>
	  </select>
      </div>
   </div>
   
   <div class="form-group has-success">
      <label class="col-sm-10 control-label" for="inputSuccess">
         Mentioning it’s a new cheaper policy from Box Protection
      </label>
      <div class="col-sm-2">
         <select type="text" class="form-control" name="mentioningitsanewcheaperpolicyfromboxprotection" id="mentioningitsanewcheaperpolicyfromboxprotection">
		 <option>0</option>
	  <option>1</option>
	  <option>2</option>
	  <option>3</option>
	  <option>4</option>
	  <option>5</option>
	  </select>
      </div>
   </div>
   <div class="form-group has-success">
      <label class="col-sm-10 control-label" for="inputWarning">
         Response to Queries of prospect(Compliant Rebuttal Used)
      </label>
      <div class="col-sm-2">
         <select type="text" class="form-control" name="properresponsetoqueriesofprospectcompliantrebuttalusedtwo" id="properresponsetoqueriesofprospectcompliantrebuttalusedtwo">
		 <option>0</option>
	  <option>1</option>
	  <option>2</option>
	  <option>3</option>
	  <option>4</option>
	  <option>5</option>
	  </select>
      </div>
   </div>
   <div class="form-group has-success">
      <label class="col-sm-10 control-label" for="inputError">
         Inform the prospect to cancel the existing policy
      </label>
      <div class="col-sm-2">
         <select type="text" class="form-control" name="informtheprospecttocanceltheexistingpolicy" id="informtheprospecttocanceltheexistingpolicy" width="10">
		 <option>0</option>
	  <option>1</option>
	  <option>2</option>
	  <option>3</option>
	  <option>4</option>
	  <option>5</option>
	  </select>
      </div>
   </div>
   
   
    <div class="form-group has-success">
      <label class="col-sm-10 control-label" for="inputSuccess">
         Prospect happy giving out the banking details
      </label>
      <div class="col-sm-2">
         <select type="text" class="form-control" name="prospecthappygivingoutthebankingdetails" id="prospecthappygivingoutthebankingdetails">
		 <option>0</option>
	  <option>1</option>
	  <option>2</option>
	  <option>3</option>
	  <option>4</option>
	  <option>5</option>
	  </select>
      </div>
   </div>
   
   <div class="form-group">
         <label for="disabledSelect"  class="col-sm-15 control-label">
		 
            <h5 class="section-title text-center wow fadeInDown" style="color:#990066"><strong>Appending Phase</strong></h5>
         </label>
         <div class="col-sm-2">
           
               
         </div>
      </div>
   <div class="form-group has-success">
      <label class="col-sm-10 control-label" for="inputWarning">
         Reading the appending statement verbatim
      </label>
      <div class="col-sm-2">
         <select type="text" class="form-control" name="readingtheappendingstatementverbatim" id="readingtheappendingstatementverbatim">
		 <option>0</option>
	  <option>1</option>
	  <option>2</option>
	  <option>3</option>
	  <option>4</option>
	  <option>5</option>
	  </select>
      </div>
   </div>
   <div class="form-group has-success">
      <label class="col-sm-10 control-label" for="inputWarning">
         Seeking Proper Acknowledgement
      </label>
      <div class="col-sm-2">
         <select type="text" class="form-control" name="seekingproperacknowledgement" id="seekingproperacknowledgement">
		 <option>0</option>
	  <option>1</option>
	  <option>2</option>
	  <option>3</option>
	  <option>4</option>
	  <option>5</option>
	  </select>
      </div>
   </div>
   <div class="form-group has-success">
      <label class="col-sm-10 control-label" for="inputError">
        Response to Queries of prospect(Compliant Rebuttal Used) 
      </label>
      <div class="col-sm-2">
         <select type="text" class="form-control" name="properresponsetoqueriesofprospectcompliantrebuttalusedthree" id="properresponsetoqueriesofprospectcompliantrebuttalusedthree" width="10">
		 <option>0</option>
	  <option>1</option>
	  <option>2</option>
	  <option>3</option>
	  <option>4</option>
	  <option>5</option>
	  </select>
      </div>
   </div>
   
    <div class="form-group has-success">
      <label  for="inputSuccess">
       <strong>  Prospect happy with the policy </strong>
      </label>
      <div class="col-sm-2">
         <select type="text" class="form-control" name="prospecthappywiththepolicy" id="prospecthappywiththepolicy">
		  <option>0</option>
	  <option>1</option>
	  <option>2</option>
	  <option>3</option>
	  <option>4</option>
	  <option>5</option>
	  </select>
      </div>
   </div>
   
   <div class="form-group has-success">
      <label class="col-sm-10 control-label" for="inputSuccess">
         
      </label>
      <div class="col-sm-2">
         
      </div>
   </div>
     <div class="form-group has-success">
      <label class="col-sm-10 control-label" for="inputError">
         
      </label>
      <div class="col-sm-2">
         
      </div>
   </div>
   <div class="form-group has-success">
         <label for="disabledSelect"  class="col-sm-4 control-label">
            <strong>STATUS</strong>
         </label>
         <div class="col-sm-8">
           <select class="form-control" id="qcstatus" name="qcstatus" value="<?php echo $qcstatus; ?>" >
	  <option>Need To Process</option>
	  <option>QCR Done</option>
   
  </select>
               
         </div>
      </div>
	  <div class="form-group has-success">
      <label class="col-sm-10 control-label" for="inputError">
         
      </label>
      <div class="col-sm-2">
         
      </div>
   </div>
   <div class="form-group has-warning">
      <label class="col-sm-7 control-label" for="inputWarning">
         
      </label>
      <div class="col-sm-5">
         <button type="submit" class="btn btn-primary" style="margin-right:200px;" >Submit</button>
      </div>
   </div>
   
   <div class="form-group has-error">
      <label class="col-sm-10 control-label" for="inputError">
         
      </label>
      <div class="col-sm-2">
         
      </div>
   </div>
   
    
   </form>


</div></div>


          
 
  
  


    

       

   	 

   