<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title></title>

</head>

<body>
<?php
extract($vcagent);
?>

<form method="post" action="<?php echo base_url();?>index.php/vcagent/update">

<div class="welcome">
<div class="container">
<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title"> <p class="style2">SALE</p></h3>
  </div>
  <div class="panel-body">
   <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Centre</strong></div>
      <input class="form-control" type="text" id="centre" name="centre"  value="<?php echo $centre; ?>" >
    </div>
  </div>
   <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Sale Date</strong></div>
      <input class="form-control" type="text" id="date" name="saledate" placeholder="click here..." value="<?php echo $saledate; ?>"  >
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Conversion Agent</strong></div>
      <input class="form-control" type="text" id="conversionagent" name="conversionagent" value="<?php echo $conversionagent; ?>"  >
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Time Of Vc Call</strong></div>
      <input class="form-control" type="text" id="timeofcall" name="timeofcall" value="<?php echo $timeofcall; ?>"  >
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Title</strong></div>
      <input class="form-control" type="text" id="title" name="title"  value="<?php echo $title; ?>"  >
    </div>
	</div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>First Name</strong></div>
      <input class="form-control" type="text" id="firstname" name="firstname"  value="<?php echo $firstname; ?>"  >
    </div>
	</div>
    <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Last Name</strong></div>
      <input class="form-control" type="text" id="lastname" name="lastname"  value="<?php echo $lastname; ?>"  >
    </div>
	</div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Phone Number</strong></div>
      <input class="form-control" type="text"  id="phonenumber" name="phonenumber"  value="<?php echo $phonenumber; ?>" >
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Address1</strong></div>
      <input class="form-control" type="text"  id="address1" name="address1"  value="<?php echo $address1; ?>" >
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Address2</strong></div>
      <input class="form-control" type="text" id="address2" name="address2"  value="<?php echo $address2; ?>" >
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Address3</strong></div>
      <input class="form-control" type="text"  id="address3" name="address3"  value="<?php echo $address3; ?>" >
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Post Code</strong></div>
      <input class="form-control" type="text"  id="postcode" name="postcode"  value="<?php echo $postcode; ?>" >
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>County</strong></div>
      <input class="form-control" type="text" id="county" name="county"  value="<?php echo $county; ?>" >
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Town</strong></div>
      <input class="form-control" type="text" id="town" name="town"  value="<?php echo $town; ?>" >
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>1st Pay Date</strong></div>
      <input class="form-control" type="text" id="datepicker" name="fstpaydate"  value="<?php echo $fstpaydate; ?>" >
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>C_Acc No.</strong></div>
      <input class="form-control" type="text"  id="c_accno" name="c_accno"  value="<?php echo $c_acc; ?>" >
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Sc_Exp No.</strong></div>
      <input class="form-control" type="text"  id="sc_expno" name="sc_expno"  value="<?php echo $sc_exp; ?>" >
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Bank Name</strong></div>
      <input class="form-control" type="text"  id="bankname" name="bankname"  value="<?php echo $bankname; ?>" >
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Bank Acc. Name</strong></div>
      <input class="form-control" type="text"  id="bankaccountname" name="bankaccountname"  value="<?php echo $bankaccountname; ?>" >
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Upsold</strong></div>
      <input class="form-control" type="text"  id="upsold" name="upsold"  value="<?php echo $upsold; ?>" >
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Box Type</strong></div>
      <input class="form-control" type="text"  id="boxtype" name="boxtype"  value="<?php echo $boxtype; ?>" >
    </div>
  </div>
  
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Frequency</strong></div>
      <input class="form-control" type="text"  id="frequency" name="frequency"  value="<?php echo $frequency; ?>" >
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Ini.Amount</strong></div>
      <input class="form-control" type="text"  id="initialamount" name="initialamount"  value="<?php echo $initialamount; ?>" >
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Password</strong></div>
      <input class="form-control" type="text"  id="password" name="password"  value="<?php echo $password; ?>" >
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Final Premium</strong></div>
      <input class="form-control" type="text"  id="finalpremium" name="finalpremium"  value="<?php echo $finalpremium; ?>" >
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Policy Number</strong></div>
      <input class="form-control" type="text"  id="policynumber" name="policynumber"  value="<?php echo $policynumber; ?>" >
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Type Of TV</strong></div>
      <input class="form-control" type="text"  id="typeoftv" name="typeoftv"  value="<?php echo $typeoftv; ?>" >
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Home Emergency</strong></div>
      <input class="form-control" id="emergency" name="home_emergency"  value="<?php echo $home_emergency; ?>" >
    </div>
  </div>
   <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>White Goods</strong></div>
      <input class="form-control" type="text"  id="wgoods" name="wgoods"  value="<?php echo $wgoods; ?>" >
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Comments</strong></div>
      <textarea class="form-control" rows="3"  id="comments" name="comments"  value="" ><?php echo $comments; ?></textarea>
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Q/C Comments</strong></div>
      <textarea class="form-control" rows="3" id="qualitycomments" name="qualitycomments" value="" disabled="disabled" ><?php echo $qualitycomments; ?></textarea>
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>status</strong></div>
      
	   <input class="form-control" type="text"  id="status" name="status" value="<?php echo $status; ?>" disabled="disabled"  >

  </div>
  </div>
   <input type="hidden" name="id" value="<?php echo $id; ?>" />
	 
	</div>
	</div>
	</div>
	</div>
	
</form>
</body>

</html>
