<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<title>SALES BOARD</title>





</head>

<body>

	
<div class="welcome">
<div class="container">
		<h1><?php echo $title; ?></h1>
		<?php echo $message; ?>
		<form method="post" action="<?php echo $action; ?>">
		<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title"> <p class="style2">Enter Your Data</p></h3>
  </div>
  
  <div class="panel-body">
    <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Name</strong></div>
      <input class="form-control" type="text" id="name" name="name" placeholder="Name" value="<?php echo set_value('name',$this->form_data->name); ?>" ><?php echo form_error('name'); ?>
    </div>
	</div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Phone Number</strong></div>
      <input class="form-control" type="text"  id="phonenumber" name="phonenumber" placeholder="Phone Number" value="<?php echo set_value('phonenumber',$this->form_data->phonenumber); ?>"><?php echo form_error('phonenumber'); ?>
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Address1</strong></div>
      <input class="form-control" type="text"  id="address1" name="address1" placeholder="Address1" value="<?php echo set_value('address1',$this->form_data->address1); ?>">
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Address2</strong></div>
      <input class="form-control" type="text" id="address2" name="address2" placeholder="Address2" value="<?php echo set_value('address2',$this->form_data->address2); ?>">
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Address3</strong></div>
      <input class="form-control" type="text"  id="address3" name="address3" placeholder="Address3" value="<?php echo set_value('address3',$this->form_data->address3); ?>">
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Post Code</strong></div>
      <input class="form-control" type="text"  id="postcode" name="postcode" placeholder="Post Code" value="<?php echo set_value('postcode',$this->form_data->postcode); ?>">
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>County</strong></div>
      <input class="form-control" type="text" id="county" name="county" placeholder="County" value="<?php echo set_value('county',$this->form_data->county); ?>">
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Town</strong></div>
      <input class="form-control" type="text" id="town" name="town" placeholder="Town" value="<?php echo set_value('town',$this->form_data->town); ?>">
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>1st Pay Date</strong></div>
      <input class="form-control" type="text" id="datepicker" name="fstpaydate" placeholder="1st Pay date" value="<?php echo set_value('fstpaydate',$this->form_data->fstpaydate); ?>">
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>C_Acc No.</strong></div>
      <input class="form-control" type="text"  id="c_accno" name="c_accno" placeholder="C_Acc No." value="<?php echo set_value('c_accno',$this->form_data->c_accno); ?>">
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Sc_Exp No.</strong></div>
      <input class="form-control" type="text"  id="sc_expno" name="sc_expno" placeholder="Sc_Exp No." value="<?php echo set_value('name',$this->form_data->sc_expno); ?>">
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Box Type</strong></div>
      <input class="form-control" type="text"  id="boxtype" name="boxtype" placeholder="Box Type" value="<?php echo set_value('name',$this->form_data->boxtype); ?>">
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Account Number</strong></div>
      <input class="form-control" type="text"  id="accountnumber" name="accountnumber" placeholder="Account Number" value="<?php echo set_value('name',$this->form_data->accountnumber); ?>">
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Frequency</strong></div>
      <input class="form-control" type="text"  id="frequency" name="frequency" placeholder="Frequency" value="<?php echo set_value('name',$this->form_data->frequency); ?>">
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Amount</strong></div>
      <input class="form-control" type="text"  id="amount" name="amount" placeholder="Amount" value="<?php echo set_value('name',$this->form_data->amount); ?>">
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Password</strong></div>
      <input class="form-control" type="text"  id="password" name="password" placeholder="Password" value="<?php echo set_value('name',$this->form_data->password); ?>">
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Product Type</strong></div>
      <input class="form-control" type="text"  id="product" name="product" placeholder="Product Type" value="<?php echo set_value('name',$this->form_data->product); ?>">
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Home Emergency</strong></div>
      <input class="form-control" id="emergency" name="home_emergency" placeholder="Home Emergency" value="<?php echo set_value('name',$this->form_data->home_emergency); ?>">
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Centre</strong></div>
      <input class="form-control" type="text" id="centre" name="centre" placeholder="Centre" value="<?php echo set_value('name',$this->form_data->centre); ?>">
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>White Goods</strong></div>
      <input class="form-control" type="text"  id="wgoods" name="wgoods" placeholder="White Goods" value="<?php echo set_value('name',$this->form_data->wgoods); ?>">
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Comments</strong></div>
      <input class="form-control" type="text"  id="comments" name="comments" placeholder="comments" value="<?php echo set_value('name',$this->form_data->comments); ?>">
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>status</strong></div>
      <select class="form-control" id="status" name="status" value="<?php echo set_value('name',$this->form_data->status); ?>"><?php echo form_error('status'); ?>
<option>call to confirm</option>
 <option>sale made</option>
  <option>call back</option>
   <option>cancelled</option>
  </select>
  </div>
  </div>
	 <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" class="btn btn-primary" style="margin-right:200px;" >Submit</button>
    </div>
  </div>
		
	</div>
</body>
</html>

