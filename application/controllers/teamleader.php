<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Teamleader extends CI_Controller{ 
	public $limit = 10;
	
	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->model('team_model','',TRUE);
		
		$this->load->library(array('table','form_validation'));
		$this->load->helper(array('form', 'url'));
		// load helper
		$this->load->helper('url');
		
		// load model
		
	}
	public function index()
	{
		$data['title']= 'Index';
		$this->load->view('templates/header_view',$data);
		$this->load->view('templates/index.php', $data);
		$this->load->view('templates/footer_view',$data);
	}


public function tlindex()
	{
		if(($this->session->userdata('usname')!=""))
		{
			$this->tlblog();
		}
		else{
			$data['title']= 'Home';
			$this->load->view('templates/header_view',$data);
			$this->load->view("teamleader/tl_admin.php", $data);
			$this->load->view('templates/footer_view',$data);
		}
	}
	
	public function tlogin()
 
  {
 $this->load->library('form_validation');
 $this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
$this->form_validation->set_rules('pass', 'Password', 'trim|required|xss_clean');

if ($this->form_validation->run() == FALSE) {

			$data['title']= 'Home';
			$this->load->view('templates/header_view',$data);
			$this->load->view("teamleader/tl_admin.php", $data);
			$this->load->view('templates/footer_view',$data);
			} 
			else 
			{
  $username=$this->input->post('username');
  $password=md5($this->input->post('pass'));

  $result=$this->team_model->tlogin($username,$password);

  if($result) { $this->tlblog(); 
  }
  else   { $msg['error_message'] =  'Invalid Username or Password';

             $this->load->view('templates/header_view');
			$this->load->view("teamleader/tl_admin.php", $msg);
			$this->load->view('templates/footer_view');
 }
 }
 }
 
 public function newlead()
 {
 
		$data['base']=$this->config->item('base_url');
		$data['title']= 'Sales Board ';
		$this->load->model("team_model");
		
		$total=$this->team_model->message_count();
		$per_pg=20;
		$offset=$this->uri->segment(3);
       			
		$this->load->library('pagination');
		$config['base_url'] = $data['base'].'/index.php/teamleader/newlead/';
	    $config['total_rows'] = $total;
	    $config['per_page'] = $per_pg;
		$config['full_tag_open'] = '<div id="pagination">';
		$config['full_tag_close'] = '</div>';
            
        $this->pagination->initialize($config);
             
        $data['pagination']=$this->pagination->create_links();
		
		
		
		$data['user_list'] = $this->team_model->get_newsale_users($per_pg,$offset);

 $data['title']= 'New lead';
  $this->load->view('templates/header_view',$data);
		$this->load->view('teamleader/nav', $data);
		$this->load->view('teamleader/newlead', $data);
		$this->load->view('templates/footer_view',$data);
 }
	
 public function tlblog()
 {
		$data['base']=$this->config->item('base_url');
		$data['title']= 'Sales Board ';
		$this->load->model("team_model");
		
		$total=$this->team_model->message_count();
		$per_pg=20;
		$offset=$this->uri->segment(3);
       			
		$this->load->library('pagination');
		$config['base_url'] = $data['base'].'/index.php/teamleader/tlblog/';
	    $config['total_rows'] = $total;
	    $config['per_page'] = $per_pg;
		$config['full_tag_open'] = '<div id="pagination">';
		$config['full_tag_close'] = '</div>';
            
        $this->pagination->initialize($config);
             
        $data['pagination']=$this->pagination->create_links();
		
		
		
		$data['user_list'] = $this->team_model->get_all_users($per_pg,$offset);


$this->load->view('templates/header_view',$data);
$this->load->view("teamleader/nav", $data);
$this->load->view("teamleader/welcome_view", $data);
$this->load->view('templates/footer_view',$data);
	}
	
	
public function edit(){
	
	
 $id = $this->uri->segment(3);
        $data['teamleader'] = $this->team_model->getById($id);
       
	    $this->load->view('templates/header_view',$data);
        $this->load->view("teamleader/nav", $data);
        $this->load->view('teamleader/personEdit', $data);
		$this->load->view('templates/footer_view',$data);
      
 }
 public function verify(){
	
	
 $id = $this->uri->segment(3);
        $data['teamleader'] = $this->team_model->getById($id);
       
	    $this->load->view('templates/header_view',$data);
        $this->load->view("teamleader/nav", $data);
        $this->load->view('teamleader/verify', $data);
		$this->load->view('templates/footer_view',$data);
      
 }
 public function approve(){
	
	
 $id = $this->uri->segment(3);
        $data['teamleader'] = $this->team_model->getById($id);
       
	    $this->load->view('templates/header_view',$data);
        $this->load->view("teamleader/nav", $data);
        $this->load->view('teamleader/approve', $data);
		$this->load->view('templates/footer_view',$data);
      
 }
 
 public function disapprove(){
	
	
 $id = $this->uri->segment(3);
        $data['teamleader'] = $this->team_model->getById($id);
       
	    $this->load->view('templates/header_view',$data);
        $this->load->view("teamleader/nav", $data);
        $this->load->view('teamleader/disapprove', $data);
		$this->load->view('templates/footer_view',$data);
      
 }
 public function cancel(){
	
	
 $id = $this->uri->segment(3);
        $data['teamleader'] = $this->team_model->getById($id);
       
	    $this->load->view('templates/header_view',$data);
        $this->load->view("teamleader/nav", $data);
        $this->load->view('teamleader/cancel', $data);
		$this->load->view('templates/footer_view',$data);
      
 }
	public function update()
	{
	$mdata['timeofcall']=$_POST['timeofcall'];
	$mdata['title']=$_POST['title'];
	$mdata['firstname']=$_POST['firstname'];
	$mdata['lastname']=$_POST['lastname'];
$mdata['phonenumber']=$_POST['phonenumber'];
$mdata['address1']=$_POST['address1'];
$mdata['address2']=$_POST['address2'];
$mdata['address3']=$_POST['address3'];
$mdata['postcode']=$_POST['postcode'];
$mdata['county']=$_POST['county'];
$mdata['town']=$_POST['town'];
$mdata['fstpaydate']=$_POST['fstpaydate'];
$mdata['c_acc']=$_POST['c_accno'];
$mdata['sc_exp']=$_POST['sc_expno'];
$mdata['boxtype']=$_POST['boxtype'];
$mdata['bankname']=$_POST['bankname'];
$mdata['bankaccountname']=$_POST['bankaccountname'];
$mdata['frequency']=$_POST['frequency'];
$mdata['amount']=$_POST['amount'];
$mdata['typeoftv']=$_POST['typeoftv'];
$mdata['home_emergency']=$_POST['home_emergency'];
$mdata['password']=$_POST['password'];
$mdata['comments']=$_POST['comments'];
$mdata['centre']=$_POST['centre'];
$mdata['wgoods']=$_POST['wgoods'];
$mdata['status']=$_POST['status'];
$mdata['status']=$_POST['status'];

$res=$this->team_model->update_info($mdata, $_POST['id']);
if($res){

header('location:'.base_url()."index.php/teamleader/tlblog".$this->tlblog());

}
}
	

	public function cancelled()
 {
 
		$data['base']=$this->config->item('base_url');
		$data['title']= 'Sales Board ';
		$this->load->model("team_model");
		
		$total=$this->team_model->message_count();
		$per_pg=20;
		$offset=$this->uri->segment(3);
       			
		$this->load->library('pagination');
		$config['base_url'] = $data['base'].'/index.php/teamleader/cancelled/';
	    $config['total_rows'] = $total;
	    $config['per_page'] = $per_pg;
		$config['full_tag_open'] = '<div id="pagination">';
		$config['full_tag_close'] = '</div>';
            
        $this->pagination->initialize($config);
             
        $data['pagination']=$this->pagination->create_links();
		
		
		
		$data['user_list'] = $this->team_model->get_call_users($per_pg,$offset);

 $data['title']= 'Cancelled';
  $this->load->view('templates/header_view',$data);
		$this->load->view('teamleader/nav', $data);
		$this->load->view('teamleader/cancelled', $data);
		$this->load->view('templates/footer_view',$data);
 }
 
 public function verified()
 {
 
		$data['base']=$this->config->item('base_url');
		$data['title']= 'Sales Board ';
		$this->load->model("team_model");
		
		$total=$this->team_model->message_count();
		$per_pg=20;
		$offset=$this->uri->segment(3);
       			
		$this->load->library('pagination');
		$config['base_url'] = $data['base'].'/index.php/teamleader/verified/';
	    $config['total_rows'] = $total;
	    $config['per_page'] = $per_pg;
		$config['full_tag_open'] = '<div id="pagination">';
		$config['full_tag_close'] = '</div>';
            
        $this->pagination->initialize($config);
             
        $data['pagination']=$this->pagination->create_links();
		
		
		
		$data['user_list'] = $this->team_model->get_verify_users($per_pg,$offset);

 $data['title']= 'Verified';
  $this->load->view('templates/header_view',$data);
		$this->load->view('teamleader/nav', $data);
		$this->load->view('teamleader/verified', $data);
		$this->load->view('templates/footer_view',$data);
 }

 public function approved()
 {
 
		$data['base']=$this->config->item('base_url');
		$data['title']= 'Sales Board ';
		$this->load->model("team_model");
		
		$total=$this->team_model->message_count();
		$per_pg=20;
		$offset=$this->uri->segment(3);
       			
		$this->load->library('pagination');
		$config['base_url'] = $data['base'].'/index.php/teamleader/approved/';
	    $config['total_rows'] = $total;
	    $config['per_page'] = $per_pg;
		$config['full_tag_open'] = '<div id="pagination">';
		$config['full_tag_close'] = '</div>';
            
        $this->pagination->initialize($config);
             
        $data['pagination']=$this->pagination->create_links();
		
		
		
		$data['user_list'] = $this->team_model->get_approve_users($per_pg,$offset);

 $data['title']= 'Approved';
  $this->load->view('templates/header_view',$data);
		$this->load->view('teamleader/nav', $data);
		$this->load->view('teamleader/approved', $data);
		$this->load->view('templates/footer_view',$data);
 }
 public function disapproved()
 {
 
		$data['base']=$this->config->item('base_url');
		$data['title']= 'Sales Board ';
		$this->load->model("team_model");
		
		$total=$this->team_model->message_count();
		$per_pg=20;
		$offset=$this->uri->segment(3);
       			
		$this->load->library('pagination');
		$config['base_url'] = $data['base'].'/index.php/teamleader/disapproved/';
	    $config['total_rows'] = $total;
	    $config['per_page'] = $per_pg;
		$config['full_tag_open'] = '<div id="pagination">';
		$config['full_tag_close'] = '</div>';
            
        $this->pagination->initialize($config);
             
        $data['pagination']=$this->pagination->create_links();
		
		
		
		$data['user_list'] = $this->team_model->get_disapprove_users($per_pg,$offset);

 $data['title']= 'Dispproved';
  $this->load->view('templates/header_view',$data);
		$this->load->view('teamleader/nav', $data);
		$this->load->view('teamleader/disapproved', $data);
		$this->load->view('templates/footer_view',$data);
 }
public function insertdata()
 {
  $this->load->library('form_validation');
  // field name, error message, validation rules
  $this->form_validation->set_rules('centre', 'Center', 'trim|required|min_length[4]|xss_clean');
  $this->form_validation->set_rules('saledate', 'Sale Date', 'trim|required|min_length[4]|xss_clean');
  $this->form_validation->set_rules('phonenumber', 'phonenumber', 'trim|required|min_length[4]|xss_clean');
  
  
  
  if($this->form_validation->run() == FALSE)
  {
   $this->tlblog();
  }
  else
  {
   $this->team_model->add_user();
   $this->thank();
  }
 }
public function thank()
	{
		$data['title']= 'Thank';
		$this->load->view('templates/header_view',$data);
		$this->load->view('teamleader/nav.php', $data);
		$this->load->view('teamleader/thank_view.php', $data);
		$this->load->view('templates/footer_view',$data);
	}
public function logout()
	{
		$newdata = array(
		'username'   =>'',
		'password'  =>'',
		
		'logged_in' => FALSE,
		);
		$this->session->unset_userdata($newdata );
		$this->session->sess_destroy();
		$this->index();
	}
}