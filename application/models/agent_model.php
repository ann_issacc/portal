<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Agent_model extends CI_Model {
    
    public function __construct()
    {
        parent::__construct();
    }
	function agentlogin($username,$password)
    {
		$this->db->where("username",$username);
        $this->db->where("password",$password);
            
        $query=$this->db->get("loginportal");
        if($query->num_rows()>0)
        {
         	foreach($query->result() as $rows)
            {
            	//add all data to session
                $newdata = array(
      
      'uname'  => $rows->username,
      
      'logged_in'  => TRUE,
    );
			}
            	$this->session->set_userdata($newdata);
                return true;            
		}
		return false;
    }
	
public function add_user()
 {
  $data=array(
    'name'=>$this->input->post('name'),
	'saledate'=>date('Y-m-d', strtotime($this->input->post('saledate'))),
    'phonenumber'=>$this->input->post('phonenumber'),
    'address1'=>($this->input->post('address1')),
	'address2'=>($this->input->post('address2')),
	'address3'=>($this->input->post('address3')),
	'postcode'=>($this->input->post('postcode')),
	'county'=>($this->input->post('county')),
	'town'=>($this->input->post('town')),
	'fstpaydate'=>($this->input->post('fstpaydate')),
	'c_acc'=>($this->input->post('c_accno')),
	'sc_exp'=>($this->input->post('sc_expno')),
	'boxtype'=>($this->input->post('boxtype')),
	'acc_no'=>($this->input->post('accountnumber')),
	'frequency'=>($this->input->post('frequency')),
	'amount'=>($this->input->post('amount')),
	'protype'=>($this->input->post('product')),
	'home_emergency'=>($this->input->post('home_emergency')),
	'password'=>($this->input->post('password')),
	'comments'=>($this->input->post('comments')),
	'centre'=>($this->input->post('centre')),
	'wgoods'=>($this->input->post('wgoods')),
	);
  $this->db->insert('agentdetails',$data);
 }
 }