<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Agent extends CI_Controller{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('agent_model');
	}
	public function index()
	{
		$data['title']= 'Index';
		$this->load->view('templates/header_view',$data);
		$this->load->view('templates/index.php', $data);
		$this->load->view('templates/footer_view',$data);
	}
	
public function agentindex()
	{
		if(($this->session->userdata('uname')!=""))
		{
			$this->welcome();
		}
		else{
			$data['title']= 'Home';
			$this->load->view('templates/header_view',$data);
			$this->load->view("agent/agent_login.php", $data);
			$this->load->view('templates/footer_view',$data);
		}
	}
	public function agentlogin()
	 {
 $this->load->library('form_validation');
 $this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
$this->form_validation->set_rules('pass', 'Password', 'trim|required|xss_clean');

if ($this->form_validation->run() == FALSE) {
$this->load->view('templates/header_view');
			$this->load->view("agent/agent_login.php");
			$this->load->view('templates/footer_view');
			} 
			else 
 {
  $username=$this->input->post('username');
  $password=md5($this->input->post('pass'));

  $result=$this->agent_model->agentlogin($username,$password);
   if($result) { $this->welcome(); 
  }
  else   { $msg['error_message'] =  'Invalid Username or Password';

             $this->load->view('templates/header_view');
			$this->load->view("agent/agent_login.php", $msg);
			$this->load->view('templates/footer_view');
 }
 }
 }
public function welcome()
	{
		$data['title']= 'Welcome';
		$this->load->view('templates/header_view',$data);
		$this->load->view('agent/nav.php', $data);
		$this->load->view('agent/welcome_view.php', $data);
		$this->load->view('templates/footer_view',$data);
	}
	public function thank()
	{
		$data['title']= 'Thank';
		$this->load->view('templates/header_view',$data);
		$this->load->view('agent/nav.php', $data);
		$this->load->view('agent/thank_view.php', $data);
		$this->load->view('templates/footer_view',$data);
	}
	public function insertdata()
 {
  $this->load->library('form_validation');
  // field name, error message, validation rules
  $this->form_validation->set_rules('centre', 'Center', 'trim|required|min_length[4]|xss_clean');
  $this->form_validation->set_rules('saledate', 'Sale Date', 'trim|required|min_length[4]|xss_clean');
  $this->form_validation->set_rules('name', 'Name', 'trim|required|min_length[4]|xss_clean');
  $this->form_validation->set_rules('phonenumber', 'phonenumber', 'trim|required|min_length[4]|xss_clean');
  
  
  
  if($this->form_validation->run() == FALSE)
  {
   $this->welcome();
  }
  else
  {
   $this->agent_model->add_user();
   $this->thank();
  }
 }
public function logout()
	{
		$newdata = array(
		'username'   =>'',
		'password'  =>'',
		
		'logged_in' => FALSE,
		);
		$this->session->unset_userdata($newdata );
		$this->session->sess_destroy();
		$this->index();
	}
}