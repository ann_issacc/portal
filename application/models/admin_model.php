<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class admin_model extends CI_Model {
 
  private $sale_table= 'agent_details';
 
    public function __construct()
    {
        parent::__construct();
    }
	function adminlogin($username,$password)
 {
  $this->db->where("username",$username);
  $this->db->where("password",$password);

  $query=$this->db->get("user");
  if($query->num_rows()>0)
  {
   foreach($query->result() as $rows)
   {
    //add all data to session
    $newdata = array(
      
      'username'  => $rows->username,
	  'password'  => $rows->password,
      
      'logged_in'  => TRUE,
    );
   }
   $this->session->set_userdata($newdata);
   return true;
  }
  return false;
 }
 public function get_all_box($per_pg,$offset)

{
$this->db->order_by('id','desc');
$query = $this->db->get('agent_details',$per_pg,$offset);

return $query->result();

}
public function get_all_spot($per_pg,$offset)

{
$this->db->order_by('id','desc');
$query = $this->db->get('vc_customers',$per_pg,$offset);

return $query->result();

}

public function insert_users_to_db($data)

{

return $this->db->insert('agent_details', $data);

}

public function getById($id){
   $query = $this->db->get_where('agent_details',array('id'=>$id));
   
   return $query->row_array();		  
 }
 
 public function vcgetById($id){
   $query = $this->db->get_where('vc_customers',array('id'=>$id));
   
   return $query->row_array();		  
 }

public function update_info($data,$id)

{

$this->db->where('agent_details.id',$id);

return $this->db->update('agent_details', $data);

}

public function upd_info($data,$id)

{

$this->db->where('vc_customers.id',$id);

return $this->db->update('vc_customers', $data);

}

public function spot_count()
	{
		return $this->db->count_all('vc_customers');
	}

public function get_all_excelbox($fdate,$tdate)

{

$dateRange = "saledate BETWEEN '$fdate%' AND '$tdate%'";

$this->db->where($dateRange, NULL, FALSE);  
$query = $this->db->get('agent_details');

return $query->result();

}

public function get_all_excelspot($fromdate,$todate)

{

$dateRange = "saledate BETWEEN '$fromdate%' AND '$todate%'";

$this->db->where($dateRange, NULL, FALSE);  
$query = $this->db->get('vc_customers');

return $query->result();

}

public function box_count()
	{
		return $this->db->count_all('agent_details');
	}
	
}