<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Vcquality extends CI_Controller{ 
	public $limit = 10;
	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->model('vcquality_model','',TRUE);
		
		$this->load->library(array('table','form_validation'));
		$this->load->helper(array('form', 'url'));
		// load helper
		$this->load->helper('url');
		
		// load model
		
	}
	public function index()
	{
		$data['title']= 'Index';
		$this->load->view('templates/header_view',$data);
		$this->load->view('templates/index.php', $data);
		$this->load->view('templates/footer_view',$data);
	}


public function vcqualityindex()
	{
		if(($this->session->userdata('usname')!=""))
		{
			$this->qcblog();
		}
		else{
			$data['title']= 'Home';
			$this->load->view('templates/header_view',$data);
			$this->load->view("vcquality/vcquality_admin.php", $data);
			$this->load->view('templates/footer_view',$data);
		}
	}
	
	public function vcqualitylogin()
 {
 $this->load->library('form_validation');
 $this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
$this->form_validation->set_rules('pass', 'Password', 'trim|required|xss_clean');

if ($this->form_validation->run() == FALSE) {
$data['title']= 'Home';
			$this->load->view('templates/header_view',$data);
			$this->load->view("vcquality/vcquality_admin.php", $data);
			$this->load->view('templates/footer_view',$data);
			} 
			else 
			{
  $username=$this->input->post('username');
  $password=md5($this->input->post('pass'));

  $result=$this->vcquality_model->vcqualitylogin($username,$password);

  if($result) { $this->vcqualityblog(); 
  }
  else   { $msg['error_message'] =  'Invalid Username or Password';

             $this->load->view('templates/header_view');
			$this->load->view("vcquality/vcquality_admin.php", $msg);
			$this->load->view('templates/footer_view');
 }
 }
 }
	
 public function vcqualityblog()
 {
		$data['base']=$this->config->item('base_url');
		$data['title']= 'Converetd Leads ';
		$this->load->model("vcquality_model");
		
		$total=$this->vcquality_model->message_count();
		$per_pg=20;
		$offset=$this->uri->segment(3);
       			
		$this->load->library('pagination');
		$config['base_url'] = $data['base'].'/index.php/vcquality/vcqualityblog/';
	    $config['total_rows'] = $total;
	    $config['per_page'] = $per_pg;
		$config['full_tag_open'] = '<div id="pagination">';
		$config['full_tag_close'] = '</div>';
            
        $this->pagination->initialize($config);
             
        $data['pagination']=$this->pagination->create_links();
		
		
		
		$data['user_list'] = $this->vcquality_model->get_all_users($per_pg,$offset);


$this->load->view('templates/header_view',$data);
$this->load->view("vcquality/nav", $data);
$this->load->view("vcquality/vcquality_blog", $data);
$this->load->view('templates/footer_view',$data);
	}
	
	
public function edit(){
	
$data['title']= 'New Lead';	
 $id = $this->uri->segment(3);
        $data['quality'] = $this->vcquality_model->getById($id);
       
	    $this->load->view('templates/header_view',$data);
        $this->load->view("vcquality/nav", $data);
        $this->load->view('vcquality/personEdit', $data);
		$this->load->view('templates/footer_view',$data);
      
 }
 public function verify(){
	
	$data['title']= 'Call Back Done';
 $id = $this->uri->segment(3);
        $data['quality'] = $this->vcquality_model->getById($id);
       
	    $this->load->view('templates/header_view',$data);
        $this->load->view("vcquality/nav", $data);
        $this->load->view('vcquality/vcquality_verify', $data);
		$this->load->view('templates/footer_view',$data);
      
 }
 
 
 
	public function update()
	{
	
$mdata['vcqualitycomments']=$_POST['vcqualitycomments'];
$mdata['status']=$_POST['status'];

$res=$this->vcquality_model->update_info($mdata, $_POST['id']);
$this->vcqualityblog();




}
	

	
	
  public function verified()
 {
 
		$data['base']=$this->config->item('base_url');
		$data['title']= 'Sales Board ';
		$this->load->model("vcquality_model");
		
		$total=$this->vcquality_model->message_count();
		$per_pg=20;
		$offset=$this->uri->segment(3);
       			
		$this->load->library('pagination');
		$config['base_url'] = $data['base'].'/index.php/vcquality/verified/';
	    $config['total_rows'] = $total;
	    $config['per_page'] = $per_pg;
		$config['full_tag_open'] = '<div id="pagination">';
		$config['full_tag_close'] = '</div>';
            
        $this->pagination->initialize($config);
             
        $data['pagination']=$this->pagination->create_links();
		
		
		
		$data['user_list'] = $this->vcquality_model->get_verify_users($per_pg,$offset);

 $data['title']= 'Verified';
  $this->load->view('templates/header_view',$data);
		$this->load->view('vcquality/nav', $data);
		$this->load->view('vcquality/vcquality_verified', $data);
		$this->load->view('templates/footer_view',$data);
 }

 


public function logout()
	{
		$newdata = array(
		'username'   =>'',
		'password'  =>'',
		
		'logged_in' => FALSE,
		);
		$this->session->unset_userdata($newdata );
		$this->session->sess_destroy();
		$this->index();
	}
}