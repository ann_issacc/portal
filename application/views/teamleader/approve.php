<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>CI Insert Form</title>

</head>

<body>
<?php
extract($teamleader);
?>

<form method="post" action="<?php echo base_url();?>index.php/teamleader/update">

<div class="welcome">
<div class="container">
<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title"> <p class="style2">Update The Sale</p></h3>
  </div>
  
  <div class="panel-body">
   <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Centre</strong></div>
      <input class="form-control" type="text" id="centre" name="centre"  value="<?php echo $centre; ?>" disabled="disabled">
    </div>
  </div>
   <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Sale Date</strong></div>
      <input class="form-control" type="text" id="date" name="saledate" placeholder="click here..." value="<?php echo $saledate; ?>" disabled="disabled" >
    </div>
  </div>
    <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Name</strong></div>
      <input class="form-control" type="text" id="name" name="name"  value="<?php echo $name; ?>" disabled="disabled" >
    </div>
	</div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Phone Number</strong></div>
      <input class="form-control" type="text"  id="phonenumber" name="phonenumber"  value="<?php echo $phonenumber; ?>" disabled="disabled">
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Address1</strong></div>
      <input class="form-control" type="text"  id="address1" name="address1"  value="<?php echo $address1; ?>" disabled="disabled">
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Address2</strong></div>
      <input class="form-control" type="text" id="address2" name="address2"  value="<?php echo $address2; ?>" disabled="disabled">
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Address3</strong></div>
      <input class="form-control" type="text"  id="address3" name="address3"  value="<?php echo $address3; ?>" disabled="disabled">
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Post Code</strong></div>
      <input class="form-control" type="text"  id="postcode" name="postcode"  value="<?php echo $postcode; ?>" disabled="disabled">
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>County</strong></div>
      <input class="form-control" type="text" id="county" name="county"  value="<?php echo $county; ?>" disabled="disabled">
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Town</strong></div>
      <input class="form-control" type="text" id="town" name="town"  value="<?php echo $town; ?>" disabled="disabled">
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>1st Pay Date</strong></div>
      <input class="form-control" type="text" id="datepicker" name="fstpaydate"  value="<?php echo $fstpaydate; ?>" disabled="disabled">
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>C_Acc No.</strong></div>
      <input class="form-control" type="text"  id="c_accno" name="c_accno"  value="<?php echo $c_acc; ?>" disabled="disabled">
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Sc_Exp No.</strong></div>
      <input class="form-control" type="text"  id="sc_expno" name="sc_expno"  value="<?php echo $sc_exp; ?>" disabled="disabled">
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Box Type</strong></div>
      <input class="form-control" type="text"  id="boxtype" name="boxtype"  value="<?php echo $boxtype; ?>" disabled="disabled">
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Account Number</strong></div>
      <input class="form-control" type="text"  id="accountnumber" name="accountnumber"  value="<?php echo $acc_no; ?>" disabled="disabled">
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Frequency</strong></div>
      <input class="form-control" type="text"  id="frequency" name="frequency"  value="<?php echo $frequency; ?>" disabled="disabled">
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Amount</strong></div>
      <input class="form-control" type="text"  id="amount" name="amount"  value="<?php echo $amount; ?>" disabled="disabled">
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Password</strong></div>
      <input class="form-control" type="text"  id="password" name="password"  value="<?php echo $password; ?>" disabled="disabled">
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Product Type</strong></div>
      <input class="form-control" type="text"  id="product" name="product"  value="<?php echo $protype; ?>" disabled="disabled">
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Home Emergency</strong></div>
      <input class="form-control" id="emergency" name="home_emergency"  value="<?php echo $home_emergency; ?>" disabled="disabled">
    </div>
  </div>
   <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>White Goods</strong></div>
      <input class="form-control" type="text"  id="wgoods" name="wgoods"  value="<?php echo $wgoods; ?>" disabled="disabled">
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Comments</strong></div>
      <input class="form-control" type="text"  id="comments" name="comments"  value="<?php echo $comments; ?>" disabled="disabled">
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Q/C Comments</strong></div>
      <input class="form-control" type="text"  id="qualitycomments" name="qualitycomments" value="<?php echo $qualitycomments; ?>" >
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Review Code</strong></div>
      <input class="form-control" type="text"  id="reviewcode" name="reviewcode" value="<?php echo $reviewcode; ?>"  >
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Call Back Status</strong></div>
      <input class="form-control" type="text"  id="callbackstatus" name="callbackstatus" value="<?php echo $callbackstatus; ?>" >
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>status</strong></div>
      <select class="form-control" id="status" name="status" value="<?php echo $status; ?>" >
	  
 <option>Approved</option>
   <option>cancelled</option>
   <option>Not Checked</option>
<option>Disapproved</option>
  </select>
  </div>
  </div>
   <input type="hidden" name="id" value="<?php echo $id; ?>" />
	 <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" class="btn btn-primary" style="margin-right:200px;" >Submit</button>
    </div>
  </div>
	</div>
	</div>
	</div>
	</div>
	
</form>
</body>

</html>
