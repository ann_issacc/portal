<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Quality extends CI_Controller{ 
	public $limit = 10;
	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->model('quality_model','',TRUE);
		
		$this->load->library(array('table','form_validation'));
		$this->load->helper(array('form', 'url'));
		// load helper
		$this->load->helper('url');
		
		// load model
		
	}
	public function index()
	{
		$data['title']= 'Index';
		$this->load->view('templates/header_view',$data);
		$this->load->view('templates/index.php', $data);
		$this->load->view('templates/footer_view',$data);
	}


public function qcindex()
	{
		if(($this->session->userdata('usname')!=""))
		{
			$this->qcblog();
		}
		else{
			$data['title']= 'Home';
			$this->load->view('templates/header_view',$data);
			$this->load->view("quality/qc_admin.php", $data);
			$this->load->view('templates/footer_view',$data);
		}
	}
	
	public function qclogin()
 	
	{
 $this->load->library('form_validation');
 $this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
$this->form_validation->set_rules('pass', 'Password', 'trim|required|xss_clean');

if ($this->form_validation->run() == FALSE) {

			$data['title']= 'Home';
			$this->load->view('templates/header_view',$data);
			$this->load->view("quality/qc_admin.php", $data);
			$this->load->view('templates/footer_view',$data);
			} 
			else 
			{
  $username=$this->input->post('username');
  $password=md5($this->input->post('pass'));

  $result=$this->quality_model->qclogin($username,$password);

  if($result) { $this->qcblog(); 
  }
  else   { $msg['error_message'] =  'Invalid Username or Password';

             $this->load->view('templates/header_view');
			$this->load->view("quality/qc_admin.php", $msg);
			$this->load->view('templates/footer_view');
 }
 }
 }
	
 public function qcblog()
 {
		$data['base']=$this->config->item('base_url');
		$data['title']= 'Sales Board ';
		$this->load->model("quality_model");
		
		$total=$this->quality_model->message_count();
		$per_pg=20;
		$offset=$this->uri->segment(3);
       			
		$this->load->library('pagination');
		$config['base_url'] = $data['base'].'/index.php/quality/qcblog/';
	    $config['total_rows'] = $total;
	    $config['per_page'] = $per_pg;
		$config['full_tag_open'] = '<div id="pagination">';
		$config['full_tag_close'] = '</div>';
            
        $this->pagination->initialize($config);
             
        $data['pagination']=$this->pagination->create_links();
		
		
		
		$data['user_list'] = $this->quality_model->get_all_users($per_pg,$offset);


$this->load->view('templates/header_view',$data);
$this->load->view("quality/nav", $data);
$this->load->view("quality/qc_blog", $data);
$this->load->view('templates/footer_view',$data);
	}
	
	
public function edit(){
	
$data['title']= 'New Lead';	
 $id = $this->uri->segment(3);
        $data['quality'] = $this->quality_model->getById($id);
       
	    $this->load->view('templates/header_view',$data);
        $this->load->view("quality/nav", $data);
        $this->load->view('quality/personEdit', $data);
		$this->load->view('templates/footer_view',$data);
		
      
 }
 public function verify(){
	
	$data['title']= 'Call Back Done';
 $id = $this->uri->segment(3);
        $data['quality'] = $this->quality_model->getById($id);
       
	    $this->load->view('templates/header_view',$data);
        $this->load->view("quality/nav", $data);
        $this->load->view('quality/verify', $data);
		$this->load->view('templates/footer_view',$data);
      
 }
 public function approve(){
	
$data['title']= 'Approved';	
 $id = $this->uri->segment(3);
        $data['quality'] = $this->quality_model->getById($id);
       
	    $this->load->view('templates/header_view',$data);
        $this->load->view("quality/nav", $data);
        $this->load->view('quality/approve', $data);
		$this->load->view('templates/footer_view',$data);
      
 }
 
 public function disapprove(){
	
	$data['title']= 'Disapproved';
 $id = $this->uri->segment(3);
        $data['quality'] = $this->quality_model->getById($id);
       
	    $this->load->view('templates/header_view',$data);
        $this->load->view("quality/nav", $data);
        $this->load->view('quality/disapprove', $data);
		$this->load->view('templates/footer_view',$data);
      
 }
 public function cancel(){
	
$data['title']= 'Cancelled';	
 $id = $this->uri->segment(3);
        $data['quality'] = $this->quality_model->getById($id);
       
	    $this->load->view('templates/header_view',$data);
        $this->load->view("quality/nav", $data);
        $this->load->view('quality/cancel', $data);
		$this->load->view('templates/footer_view',$data);
      
 }
 
 
	 public function update()
	{
	
$mdata['timeofcall']=$_POST['timeofcall'];
$mdata['title']=$_POST['title'];
$mdata['firstname']=$_POST['firstname'];
$mdata['lastname']=$_POST['lastname'];
$mdata['phonenumber']=$_POST['phonenumber'];
$mdata['address1']=$_POST['address1'];
$mdata['address2']=$_POST['address2'];
$mdata['address3']=$_POST['address3'];
$mdata['postcode']=$_POST['postcode'];
$mdata['county']=$_POST['county'];
$mdata['town']=$_POST['town'];
$mdata['fstpaydate']=$_POST['fstpaydate'];
$mdata['c_acc']=$_POST['c_accno'];
$mdata['sc_exp']=$_POST['sc_expno'];
$mdata['boxtype']=$_POST['boxtype'];
$mdata['bankname']=$_POST['bankname'];
$mdata['bankaccountname']=$_POST['bankaccountname'];
$mdata['frequency']=$_POST['frequency'];
$mdata['amount']=$_POST['amount'];
$mdata['typeoftv']=$_POST['typeoftv'];
$mdata['home_emergency']=$_POST['home_emergency'];
$mdata['password']=$_POST['password'];
$mdata['comments']=$_POST['comments'];
$mdata['centre']=$_POST['centre'];
$mdata['wgoods']=$_POST['wgoods'];
$mdata['qualitycomments']=$_POST['qualitycomments'];
$mdata['qccode']=$_POST['qccode'];
$mdata['status']=$_POST['status'];

$res=$this->quality_model->update_info($mdata, $_POST['id']);
header('location:'.base_url()."index.php/quality/qcblog".$this->qcblog());
$this->quality_model->add_qcr();
{
$status = $this->input->post('status');
if($status == 'Approved')
{
$this->quality_model->add_user();
}
}



}
 
	

	
	// date_validation callback
	public function cancelled()
 {
 
		$data['base']=$this->config->item('base_url');
		$data['title']= 'Sales Board ';
		$this->load->model("quality_model");
		
		$total=$this->quality_model->message_count();
		$per_pg=20;
		$offset=$this->uri->segment(3);
       			
		$this->load->library('pagination');
		$config['base_url'] = $data['base'].'/index.php/quality/cancelled/';
	    $config['total_rows'] = $total;
	    $config['per_page'] = $per_pg;
		$config['full_tag_open'] = '<div id="pagination">';
		$config['full_tag_close'] = '</div>';
            
        $this->pagination->initialize($config);
             
        $data['pagination']=$this->pagination->create_links();
		
		
		
		$data['user_list'] = $this->quality_model->get_call_users($per_pg,$offset);

 $data['title']= 'Cancelled';
  $this->load->view('templates/header_view',$data);
		$this->load->view('quality/nav', $data);
		$this->load->view('quality/cancelled', $data);
		$this->load->view('templates/footer_view',$data);
 }
  public function verified()
 {
 
		$data['base']=$this->config->item('base_url');
		$data['title']= 'Sales Board ';
		$this->load->model("quality_model");
		
		$total=$this->quality_model->message_count();
		$per_pg=20;
		$offset=$this->uri->segment(3);
       			
		$this->load->library('pagination');
		$config['base_url'] = $data['base'].'/index.php/quality/verified/';
	    $config['total_rows'] = $total;
	    $config['per_page'] = $per_pg;
		$config['full_tag_open'] = '<div id="pagination">';
		$config['full_tag_close'] = '</div>';
            
        $this->pagination->initialize($config);
             
        $data['pagination']=$this->pagination->create_links();
		
		
		
		$data['user_list'] = $this->quality_model->get_verify_users($per_pg,$offset);

 $data['title']= 'Verified';
  $this->load->view('templates/header_view',$data);
		$this->load->view('quality/nav', $data);
		$this->load->view('quality/verified', $data);
		$this->load->view('templates/footer_view',$data);
 }

 public function approved()
 {
 
		$data['base']=$this->config->item('base_url');
		$data['title']= 'Sales Board ';
		$this->load->model("quality_model");
		
		$total=$this->quality_model->message_count();
		$per_pg=20;
		$offset=$this->uri->segment(3);
       			
		$this->load->library('pagination');
		$config['base_url'] = $data['base'].'/index.php/quality/approved/';
	    $config['total_rows'] = $total;
	    $config['per_page'] = $per_pg;
		$config['full_tag_open'] = '<div id="pagination">';
		$config['full_tag_close'] = '</div>';
            
        $this->pagination->initialize($config);
             
        $data['pagination']=$this->pagination->create_links();
		
		
		
		$data['user_list'] = $this->quality_model->get_approve_users($per_pg,$offset);

 $data['title']= 'Approved';
  $this->load->view('templates/header_view',$data);
		$this->load->view('quality/nav', $data);
		$this->load->view('quality/approved', $data);
		$this->load->view('templates/footer_view',$data);
 }
 public function disapproved()
 {
 
		$data['base']=$this->config->item('base_url');
		$data['title']= 'Sales Board ';
		$this->load->model("quality_model");
		
		$total=$this->quality_model->message_count();
		$per_pg=20;
		$offset=$this->uri->segment(3);
       			
		$this->load->library('pagination');
		$config['base_url'] = $data['base'].'/index.php/quality/disapproved/';
	    $config['total_rows'] = $total;
	    $config['per_page'] = $per_pg;
		$config['full_tag_open'] = '<div id="pagination">';
		$config['full_tag_close'] = '</div>';
            
        $this->pagination->initialize($config);
             
        $data['pagination']=$this->pagination->create_links();
		
		
		
		$data['user_list'] = $this->quality_model->get_disapprove_users($per_pg,$offset);

 $data['title']= 'Dispproved';
  $this->load->view('templates/header_view',$data);
		$this->load->view('quality/nav', $data);
		$this->load->view('quality/disapproved', $data);
		$this->load->view('templates/footer_view',$data);
 }


public function logout()
	{
		$newdata = array(
		'username'   =>'',
		'password'  =>'',
		
		'logged_in' => FALSE,
		);
		$this->session->unset_userdata($newdata );
		$this->session->sess_destroy();
		$this->index();
	}
}