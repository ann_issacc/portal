<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class super_model extends CI_Model {
 
  
 
    public function __construct()
    {
        parent::__construct();
    }
	function superlogin($username,$password)
 {
  $this->db->where("username",$username);
  $this->db->where("password",$password);

  $query=$this->db->get("user");
  if($query->num_rows()>0)
  {
   foreach($query->result() as $rows)
   {
    //add all data to session
    $newdata = array(
      
      'username'  => $rows->username,
	  'password'  => $rows->password,
      
      'logged_in'  => TRUE,
    );
   }
   $this->session->set_userdata($newdata);
   return true;
  }
  return false;
 }

	public function get_all_users($per_pg,$offset)

{
$this->db->order_by('id','desc');

$query = $this->db->get('agentdetails',$per_pg,$offset);

return $query->result();

}

public function insert_users_to_db($data)

{

return $this->db->insert('agentdetails', $data);

}

public function getById($id){
   $query = $this->db->get_where('agentdetails',array('id'=>$id));
   
   return $query->row_array();		  
 }

public function update_info($data,$id)

{

$this->db->where('agentdetails.id',$id);

return $this->db->update('agentdetails', $data);

}
public function get_sale_users($per_pg,$offset)

{
$this->db->order_by('id','desc');
$this->db->where('status', 'sale'); 
$query = $this->db->get('agentdetails',$per_pg,$offset);

return $query->result();

}
public function get_upsold_users($per_pg,$offset)

{
$this->db->order_by('id','desc');
$this->db->where('status', 'sale and upsold'); 
$query = $this->db->get('agentdetails',$per_pg,$offset);

return $query->result();

}
public function get_notmislead_users($per_pg,$offset)

{
$this->db->order_by('id','desc');
$this->db->where('status', 'declined(not mislead)'); 
$query = $this->db->get('agentdetails',$per_pg,$offset);

return $query->result();

}
public function get_mislead_users($per_pg,$offset)

{
$this->db->order_by('id','desc');
$this->db->where('status', 'declined(mislead)'); 
$query = $this->db->get('agentdetails',$per_pg,$offset);

return $query->result();

}
public function get_automated_users($per_pg,$offset)

{
$this->db->order_by('id','desc');
$this->db->where('status', 'automated response'); 
$query = $this->db->get('agentdetails',$per_pg,$offset);

return $query->result();

}
public function get_letter_users($per_pg,$offset)

{
$this->db->order_by('id','desc');
$this->db->where('status', 'send letter'); 
$query = $this->db->get('agentdetails',$per_pg,$offset);

return $query->result();

}
public function get_tcf_users($per_pg,$offset)

{
$this->db->order_by('id','desc');
$this->db->where('status', 'tcf'); 
$query = $this->db->get('agentdetails',$per_pg,$offset);

return $query->result();

}
public function message_count()
	{
		return $this->db->count_all('agentdetails');
	}
	public function get_call_users($per_pg,$offset)

{
$this->db->order_by('id','desc');
$this->db->where('status', 'call back'); 
$query = $this->db->get('agentdetails',$per_pg,$offset);

return $query->result();

}

	
}