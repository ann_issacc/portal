<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title></title>

<style type="text/css">
<!--
.style1 {color: #990000}
-->
</style>
</head>
<body>

<?php
extract($quality);
?>

<form method="post" action="<?php echo base_url();?>index.php/quality/update">
<div class="col-xs-6">
<div class="welcome1">
<div class="container1">
<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title"> <p class="style2">Update The Sale</p></h3>
  </div>
  
   <div class="panel-body1">
   <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Centre</strong></div>
      <input class="form-control" type="text" id="centre" name="centre"  value="<?php echo $centre; ?>" >
    </div>
  </div>
   <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Sale Date</strong></div>
      <input class="form-control" type="text" id="date" name="saledate"  value="<?php echo $saledate; ?>"  >
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Time Of Call</strong></div>
      <input class="form-control" type="text" id="timeofcall" name="timeofcall"  value="<?php echo $timeofcall; ?>"  >
    </div>
	 </div>
    <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Title</strong></div>
      <input class="form-control" type="text" id="title" name="title"  value="<?php echo $title; ?>"  >
    </div>
	</div>
 
    <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>First Name</strong></div>
      <input class="form-control" type="text" id="firstname" name="firstname"  value="<?php echo $firstname; ?>"  >
    </div>
	</div>
	<div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Last Name</strong></div>
      <input class="form-control" type="text" id="lastname" name="lastname"  value="<?php echo $lastname; ?>"  >
    </div>
	</div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Phone Number</strong></div>
      <input class="form-control" type="text"  id="phonenumber" name="phonenumber"  value="<?php echo $phonenumber; ?>" >
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Address1</strong></div>
      <input class="form-control" type="text"  id="address1" name="address1"  value="<?php echo $address1; ?>" >
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Address2</strong></div>
      <input class="form-control" type="text" id="address2" name="address2"  value="<?php echo $address2; ?>" >
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Address3</strong></div>
      <input class="form-control" type="text"  id="address3" name="address3"  value="<?php echo $address3; ?>" >
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Post Code</strong></div>
      <input class="form-control" type="text"  id="postcode" name="postcode"  value="<?php echo $postcode; ?>" >
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>County</strong></div>
      <input class="form-control" type="text" id="county" name="county"  value="<?php echo $county; ?>" >
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Town</strong></div>
      <input class="form-control" type="text" id="town" name="town"  value="<?php echo $town; ?>" >
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>1st Pay Date</strong></div>
      <input class="form-control" type="text" id="datepicker" name="fstpaydate"  value="<?php echo $fstpaydate; ?>" >
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Bank Name</strong></div>
      <input class="form-control" type="text"  id="bankname" name="bankname"  value="<?php echo $bankname; ?>" >
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Bank Acc. Name</strong></div>
      <input class="form-control" type="text"  id="bankaccountname" name="bankaccountname"  value="<?php echo $bankaccountname; ?>" >
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Box Type</strong></div>
      <input class="form-control" type="text"  id="boxtype" name="boxtype"  value="<?php echo $boxtype; ?>" >
    </div>
  </div>
    <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Frequency</strong></div>
      <input class="form-control" type="text"  id="frequency" name="frequency"  value="<?php echo $frequency; ?>" >
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Amount</strong></div>
      <input class="form-control" type="text"  id="amount" name="amount"  value="<?php echo $amount; ?>" >
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Password</strong></div>
      <input class="form-control" type="text"  id="password" name="password"  value="<?php echo $password; ?>" >
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Type Of TV</strong></div>
      <input class="form-control" type="text"  id="typeoftv" name="typeoftv"  value="<?php echo $typeoftv; ?>" >
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Home Emergency</strong></div>
      <input class="form-control" id="emergency" name="home_emergency"  value="<?php echo $home_emergency; ?>" >
    </div>
  </div>
   <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>White Goods</strong></div>
      <input class="form-control" type="text"  id="wgoods" name="wgoods"  value="<?php echo $wgoods; ?>" >
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Comments</strong></div>
      <textarea class="form-control" rows="3"  id="comments" name="comments"  value="" ><?php echo $comments; ?></textarea>
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon style1"><strong>Q/C Comments</strong></div>
      <textarea class="form-control" rows="3"   id="qualitycomments" name="qualitycomments" value="" ><?php echo $qualitycomments; ?></textarea>
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon style1"><strong>Q/C Review Code</strong></div>
      <input class="form-control" type="text"  id="qccode" name="qccode" value="<?php echo $qccode; ?>"  >
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>status</strong></div>
      <select class="form-control" id="status" name="status" value="<?php echo $status; ?>" >
	  <option>New Lead</option>
	  <option>Approved</option>
 <option>Disapproved</option>
   <option>cancelled</option>
  </select>
  </div>
  </div>
   <input type="hidden" name="id" value="<?php echo $id; ?>" />
	 <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" class="btn btn-primary" style="margin-right:200px;" >Submit</button>
    </div>
  </div>
	</div>
	</div>
	</div>
	</div>
	  
      <input type="hidden" class="form-control" type="text"  id="c_accno" name="c_accno"  value="<?php echo $c_acc; ?>"  >
    
      
      <input type="hidden" class="form-control" type="text"  id="sc_expno" name="sc_expno"  value="<?php echo $sc_exp; ?>" >
    
	</div>
	

 <div class="col-xs-6">
<div class="welcome2">
<div class="container2">
<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title"> <p class="style2" style="color:#990066">QCR Update</p></h3>
  </div>

   <div class="panel-body2">
 <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>ID</strong></div>
        <input class="form-control" type="text" id="id" name="id"  value="<?php echo $id; ?>" >
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>QC Agent</strong></div>
        <input type="text"  type="text" id="qcagentname" name="qcagentname"  value="<?php echo $this->session->userdata('usname'); ?>" class="form-control">
    </div>
	 </div>
	 <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Monitoring Date</strong></div>
         <input class="form-control" type="text" id="date" name="monitoringdate"  value="<?php echo date("m/d/Y") ?>"  >
    </div>
	 </div>
	     
	  
	  <div class="form-group">
         <label for="disabledSelect"  class="col-sm-4 control-label">
            
         </label>
         <div class="col-sm-8">
           
               
         </div>
      </div>
	   <div class="form-group">
         <label for="disabledSelect"  class="col-sm-15 control-label">
		 
            <h4 class="section-title text-center wow fadeInDown" style="color:#CC0033"><strong>Parameters</strong></h4>
         </label>
         <div class="col-sm-2">
           
               
         </div>
      </div>
	  <div class="form-group">
         <label for="disabledSelect"  class="col-sm-15 control-label">
		 
            <h5 class="section-title text-center wow fadeInDown" style="color:#990066"><strong>Prequalification</strong></h5>
         </label>
         <div class="col-sm-2">
           
               
         </div>
      </div>
   <div class="form-group has-success">
      <label class="col-sm-8 control-label" for="inputSuccess">
         Mentioning Company Name
      </label>
      <div class="col-sm-2">
         <select type="text" class="form-control" name="mentioningcompanyname" id="mentioningcompanyname">
		 <option>0</option>
	  <option>1</option>
	  <option>2</option>
	  <option>3</option>
	  <option>4</option>
	  <option>5</option>
	  </select>
      </div>
   </div>
      <div class="form-group">
         <label for="disabledSelect"  class="col-sm-8 control-label">
		 
            <h5 class="section-title text-center wow fadeInDown" style="color:#990066"><strong></strong></h5>
         </label>
         <div class="col-sm-2">
           
               
         </div>
      </div>
    <div class="form-group has-success">
      <label class="col-sm-8 control-label" for="inputSuccess">
         Firm confirmation from Prospect regarding existing cover
      </label>
      <div class="col-sm-2">
         <select type="text" class="form-control" name="firmconfirmationfromprospectregardingexistingcover" id="firmconfirmationfromprospectregardingexistingcover">
		 <option>0</option>
	  <option>1</option>
	  <option>2</option>
	  <option>3</option>
	  <option>4</option>
	  <option>5</option>
	  </select>
      </div>
   </div>
   <div class="form-group">
         <label for="disabledSelect"  class="col-sm-8 control-label">
		 
            <h5 class="section-title text-center wow fadeInDown" style="color:#990066"><strong></strong></h5>
         </label>
         <div class="col-sm-2">
           
               
         </div>
      </div>
   <div class="form-group has-success">
      <label class="col-sm-8 control-label" for="inputError">
         Age & Type of Equipment
      </label>
      <div class="col-sm-2">
         <select type="text" class="form-control" name="ageandtypeofequipment" id="ageandtypeofequipment" width="10">
		 <option>0</option>
	  <option>1</option>
	  <option>2</option>
	  <option>3</option>
	  <option>4</option>
	  <option>5</option>
	  </select>
      </div>
   </div>
   <div class="form-group">
         <label for="disabledSelect"  class="col-sm-8 control-label">
		 
            <h5 class="section-title text-center wow fadeInDown" style="color:#990066"><strong></strong></h5>
         </label>
         <div class="col-sm-2">
           
               
         </div>
      </div>
   <div class="form-group has-success">
      <label class="col-sm-8 control-label" for="inputSuccess">
         Proper reason of the call mentioned along with Company Name
      </label>
      <div class="col-sm-2">
         <select type="text" class="form-control" name="properreasonofthecallmentionedalongwithcompanyname" id="properreasonofthecallmentionedalongwithcompanyname">
		 <option>0</option>
	  <option>1</option>
	  <option>2</option>
	  <option>3</option>
	  <option>4</option>
	  <option>5</option>
	  </select>
      </div>
   </div>
   <div class="form-group">
         <label for="disabledSelect"  class="col-sm-8 control-label">
		 
            <h5 class="section-title text-center wow fadeInDown" style="color:#990066"><strong></strong></h5>
         </label>
         <div class="col-sm-2">
           
               
         </div>
      </div>
   <div class="form-group has-success">
      <label class="col-sm-8 control-label" for="inputWarning">
        Response to Queries of prospect (Compliant Rebuttal Used)
      </label>
      <div class="col-sm-2">
         <select type="text" class="form-control" name="Properresponsetoqueriesofprospectcompliantrebuttalusedone" id="Properresponsetoqueriesofprospectcompliantrebuttalusedone">
		 <option>0</option>
	  <option>1</option>
	  <option>2</option>
	  <option>3</option>
	  <option>4</option>
	  <option>5</option>
	  </select>
      </div>
   </div>
   <div class="form-group">
         <label for="disabledSelect"  class="col-sm-8 control-label">
		 
            <h5 class="section-title text-center wow fadeInDown" style="color:#990066"><strong>Collecting Banking Details</strong></h5>
         </label>
         <div class="col-sm-2">
           
               
         </div>
      </div>
   <div class="form-group has-success">
      <label class="col-sm-8 control-label" for="inputError">
         No Pressurizing /Badgering
      </label>
      <div class="col-sm-2">
         <select type="text" class="form-control" name="nopressurizingbadgering" id="nopressurizingbadgering" width="10">
		 <option>0</option>
	  <option>1</option>
	  <option>2</option>
	  <option>3</option>
	  <option>4</option>
	  <option>5</option>
	  </select>
      </div>
   </div>
   <div class="form-group">
         <label for="disabledSelect"  class="col-sm-8 control-label">
		 
            <h5 class="section-title text-center wow fadeInDown" style="color:#990066"><strong></strong></h5>
         </label>
         <div class="col-sm-2">
           
               
         </div>
      </div>
   <div class="form-group has-success">
      <label class="col-sm-8 control-label" for="inputSuccess">
         Mentioning it’s a new cheaper policy from Box Protection
      </label>
      <div class="col-sm-2">
         <select type="text" class="form-control" name="mentioningitsanewcheaperpolicyfromboxprotection" id="mentioningitsanewcheaperpolicyfromboxprotection">
		 <option>0</option>
	  <option>1</option>
	  <option>2</option>
	  <option>3</option>
	  <option>4</option>
	  <option>5</option>
	  </select>
      </div>
   </div>
   <div class="form-group">
         <label for="disabledSelect"  class="col-sm-8 control-label">
		 
            <h5 class="section-title text-center wow fadeInDown" style="color:#990066"><strong></strong></h5>
         </label>
         <div class="col-sm-2">
           
               
         </div>
      </div>
   <div class="form-group has-success">
      <label class="col-sm-8 control-label" for="inputWarning">
         Response to Queries of prospect(Compliant Rebuttal Used)
      </label>
      <div class="col-sm-2">
         <select type="text" class="form-control" name="properresponsetoqueriesofprospectcompliantrebuttalusedtwo" id="properresponsetoqueriesofprospectcompliantrebuttalusedtwo">
		 <option>0</option>
	  <option>1</option>
	  <option>2</option>
	  <option>3</option>
	  <option>4</option>
	  <option>5</option>
	  </select>
      </div>
   </div>
   <div class="form-group">
         <label for="disabledSelect"  class="col-sm-8 control-label">
		 
            <h5 class="section-title text-center wow fadeInDown" style="color:#990066"><strong></strong></h5>
         </label>
         <div class="col-sm-2">
           
               
         </div>
      </div>
   <div class="form-group has-success">
      <label class="col-sm-8 control-label" for="inputError">
         Inform the prospect to cancel the existing policy
      </label>
      <div class="col-sm-2">
         <select type="text" class="form-control" name="informtheprospecttocanceltheexistingpolicy" id="informtheprospecttocanceltheexistingpolicy" width="10">
		 <option>0</option>
	  <option>1</option>
	  <option>2</option>
	  <option>3</option>
	  <option>4</option>
	  <option>5</option>
	  </select>
      </div>
   </div>
   
   <div class="form-group">
         <label for="disabledSelect"  class="col-sm-8 control-label">
		 
            <h5 class="section-title text-center wow fadeInDown" style="color:#990066"><strong></strong></h5>
         </label>
         <div class="col-sm-2">
           
               
         </div>
      </div>
    <div class="form-group has-success">
      <label class="col-sm-8 control-label" for="inputSuccess">
         Prospect happy giving out the banking details
      </label>
      <div class="col-sm-2">
         <select type="text" class="form-control" name="prospecthappygivingoutthebankingdetails" id="prospecthappygivingoutthebankingdetails">
		 <option>0</option>
	  <option>1</option>
	  <option>2</option>
	  <option>3</option>
	  <option>4</option>
	  <option>5</option>
	  </select>
      </div>
   </div>
   <div class="form-group">
         <label for="disabledSelect"  class="col-sm-8 control-label">
		 
            <h5 class="section-title text-center wow fadeInDown" style="color:#990066"><strong></strong></h5>
         </label>
         <div class="col-sm-2">
           
               
         </div>
      </div>
   <div class="form-group">
         <label for="disabledSelect"  class="col-sm-8 control-label">
		 
            <h5 class="section-title text-center wow fadeInDown" style="color:#990066"><strong>Appending Phase</strong></h5>
         </label>
         <div class="col-sm-2">
           
               
         </div>
      </div>
   <div class="form-group has-success">
      <label class="col-sm-8 control-label" for="inputWarning">
         Reading the appending statement verbatim
      </label>
      <div class="col-sm-2">
         <select type="text" class="form-control" name="readingtheappendingstatementverbatim" id="readingtheappendingstatementverbatim">
		 <option>0</option>
	  <option>1</option>
	  <option>2</option>
	  <option>3</option>
	  <option>4</option>
	  <option>5</option>
	  </select>
      </div>
   </div>
   <div class="form-group">
         <label for="disabledSelect"  class="col-sm-8 control-label">
		 
            <h5 class="section-title text-center wow fadeInDown" style="color:#990066"><strong></strong></h5>
         </label>
         <div class="col-sm-2">
           
               
         </div>
      </div>
   <div class="form-group has-success">
      <label class="col-sm-8 control-label" for="inputWarning">
         Seeking Proper Acknowledgement
      </label>
      <div class="col-sm-2">
         <select type="text" class="form-control" name="seekingproperacknowledgement" id="seekingproperacknowledgement">
		 <option>0</option>
	  <option>1</option>
	  <option>2</option>
	  <option>3</option>
	  <option>4</option>
	  <option>5</option>
	  </select>
      </div>
   </div>
   <div class="form-group">
         <label for="disabledSelect"  class="col-sm-8 control-label">
		 
            <h5 class="section-title text-center wow fadeInDown" style="color:#990066"><strong></strong></h5>
         </label>
         <div class="col-sm-2">
           
               
         </div>
      </div>
   <div class="form-group has-success">
      <label class="col-sm-8 control-label" for="inputError">
        Response to Queries of prospect(Compliant Rebuttal Used) 
      </label>
      <div class="col-sm-2">
         <select type="text" class="form-control" name="properresponsetoqueriesofprospectcompliantrebuttalusedthree" id="properresponsetoqueriesofprospectcompliantrebuttalusedthree" width="10">
		 <option>0</option>
	  <option>1</option>
	  <option>2</option>
	  <option>3</option>
	  <option>4</option>
	  <option>5</option>
	  </select>
      </div>
   </div>
   <div class="form-group">
         <label for="disabledSelect"  class="col-sm-8 control-label">
		 
            <h5 class="section-title text-center wow fadeInDown" style="color:#990066"><strong></strong></h5>
         </label>
         <div class="col-sm-2">
           
               
         </div>
      </div>
    <div class="form-group has-success">
      <label class="col-sm-8 control-label" for="inputError">
       <strong>Prospect happy with the policy </strong>
      </label>
      <div class="col-sm-2">
         <select type="text" class="form-control" name="prospecthappywiththepolicy" id="prospecthappywiththepolicy">
		  <option>0</option>
	  <option>1</option>
	  <option>2</option>
	  <option>3</option>
	  <option>4</option>
	  <option>5</option>
	  </select>
      </div>
   </div>
   <div class="form-group">
         <label for="disabledSelect"  class="col-sm-8 control-label">
		 
            <h5 class="section-title text-center wow fadeInDown" style="color:#990066"><strong></strong></h5>
         </label>
         <div class="col-sm-2">
           
               
         </div>
      </div>
   <div class="form-group has-success">
      <label class="col-sm-8 control-label" for="inputSuccess">
         
      </label>
      <div class="col-sm-2">
         
      </div>
   </div>
     <div class="form-group has-success">
      <label class="col-sm-10 control-label" for="inputError">
         
      </label>
      <div class="col-sm-2">
         
      </div>
   </div>
   
               
         </div>
      </div>
         </div>
      </div>
	  </div>
	</div>
	</div>
	
	</div>
	</div>
	
</form>
</body>
