
<div class="welcome">
<div class="container">

 <?php echo form_open("agent/insertdata"); ?>

<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title"> <p class="style2">Enter Your Sale</p></h3>
  </div><?php echo validation_errors('<p class="error">'); ?>
  <div class="panel-body">
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Centre</strong></div>
      <input class="form-control" type="text" id="centre" name="centre" placeholder="Type Your Center Name" >
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Sale Date</strong></div>
      <input class="form-control" type="text" id="date" name="saledate" placeholder="click here..." value="<?php echo date("d-m-Y") ?>" >
    </div>
  </div>
    <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Name</strong></div>
      <input class="form-control" type="text" id="name" name="name"  >
    </div>
	</div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Phone Number</strong></div>
      <input class="form-control" type="text"  id="phonenumber" name="phonenumber" >
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Address1</strong></div>
      <input class="form-control" type="text"  id="address1" name="address1" >
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Address2</strong></div>
      <input class="form-control" type="text" id="address2" name="address2" >
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Address3</strong></div>
      <input class="form-control" type="text"  id="address3" name="address3" >
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Post Code</strong></div>
      <input class="form-control" type="text"  id="postcode" name="postcode" >
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>County</strong></div>
      <input class="form-control" type="text" id="county" name="county" >
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Town</strong></div>
      <input class="form-control" type="text" id="town" name="town" >
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>1st Pay Date</strong></div>
      <input class="form-control" type="text" id="datepicker" name="fstpaydate" placeholder="click here..." >
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>C_Acc No.</strong></div>
      <input class="form-control" type="text"  id="c_accno" name="c_accno" >
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Sc_Exp No.</strong></div>
      <input class="form-control" type="text"  id="sc_expno" name="sc_expno" >
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Box Type</strong></div>
      <input class="form-control" type="text"  id="boxtype" name="boxtype" >
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Frequency</strong></div>
      <input class="form-control" type="text"  id="frequency" name="frequency" >
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Amount</strong></div>
      <input class="form-control" type="text"  id="amount" name="amount" >
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Password</strong></div>
      <input class="form-control" type="text"  id="password" name="password" >
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>TV Type</strong></div>
      <input class="form-control" type="text"  id="product" name="product" >
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Home Emergency</strong></div>
      <input class="form-control" id="emergency" name="home_emergency" >
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>White Goods</strong></div>
      <input class="form-control" type="text"  id="wgoods" name="wgoods" >
    </div>
  </div>
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon"><strong>Comments</strong></div>
      <input class="form-control" type="text"  id="comments" name="comments" >
    </div>
  </div>
 <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" class="btn btn-primary" style="margin-right:200px;" >Submit</button>
    </div>
  </div>
  <input type="hidden"  id="qualitycomments" name="qualitycomments"  >
   <input type="hidden"  id="reviewcode" name="reviewcode"  >
   <input type="hidden"  id="callbackstatus" name="callbackstatus"  >
   <input type="hidden"  id="accountnumber" name="accountnumber" >
  <?php echo form_close(); ?>
  </form>
    </div>
</div>
    
  </div>
</div>
</div><!--<div class="content">-->
