<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller{ 
	
	private $limit = 10;
	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->model('admin_model','',TRUE);
		
		$this->load->library(array('table','form_validation'));
		
		$this->load->library("excel");
		// load helper
		$this->load->helper('url');
		
		// load model
		
	}
	public function index()
	{
		$data['title']= 'Index';
		$this->load->view('templates/header_view',$data);
		$this->load->view('templates/index.php', $data);
		$this->load->view('templates/footer_view',$data);
	}


public function adminindex()
	{
		if(($this->session->userdata('username')!=""))
		{
			$this->boxblog();
		}
		else{
			$data['title']= 'Home';
			$this->load->view('templates/header_view',$data);
			$this->load->view("admin/admin_login.php", $data);
			$this->load->view('templates/footer_view',$data);
		}
	}
	
	public function adminlogin()
	{
 $this->load->library('form_validation');
 $this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
$this->form_validation->set_rules('pass', 'Password', 'trim|required|xss_clean');

if ($this->form_validation->run() == FALSE) {

			$data['title']= 'Home';
			$this->load->view('templates/header_view',$data);
			$this->load->view("admin/admin_login.php", $data);
			$this->load->view('templates/footer_view',$data);
			} 
			else 
			{
  $username=$this->input->post('username');
  $password=md5($this->input->post('pass'));

  $result=$this->admin_model->adminlogin($username,$password);

  if($result) { $this->boxblog(); 
  }
  else   { $msg['error_message'] =  'Invalid Username or Password';

             $this->load->view('templates/header_view');
			$this->load->view("admin/admin_login.php", $msg);
			$this->load->view('templates/footer_view');
 }
 }
 }
	
 public function boxblog()
 {
 $data['base']=$this->config->item('base_url');
		$data['title']= 'Lead Generation';
		$this->load->model("admin_model");
		
		$total=$this->admin_model->box_count();
		$per_pg=20;
		$offset=$this->uri->segment(3);
       			
		$this->load->library('pagination');
		$config['base_url'] = $data['base'].'/index.php/admin/boxblog/';
	    $config['total_rows'] = $total;
	    $config['per_page'] = $per_pg;
		$config['full_tag_open'] = '<div id="pagination">';
		$config['full_tag_close'] = '</div>';
            
        $this->pagination->initialize($config);
             
        $data['pagination']=$this->pagination->create_links();
		
		
		
		$data['user_list'] = $this->admin_model->get_all_box($per_pg,$offset);


$this->load->view('templates/header_view',$data);
$this->load->view("admin/nav", $data);
$this->load->view("admin/admin_blog", $data);
$this->load->view('templates/footer_view',$data);
	}
	
	 public function spotblog()
 {
 
		$data['base']=$this->config->item('base_url');
		$data['title']= 'Sales Board ';
		$this->load->model("admin_model");
		
		$total=$this->admin_model->spot_count();
		$per_pg=20;
		$offset=$this->uri->segment(3);
       			
		$this->load->library('pagination');
		$config['base_url'] = $data['base'].'/index.php/admin/spotblog/';
	    $config['total_rows'] = $total;
	    $config['per_page'] = $per_pg;
		$config['full_tag_open'] = '<div id="pagination">';
		$config['full_tag_close'] = '</div>';
            
        $this->pagination->initialize($config);
             
        $data['pagination']=$this->pagination->create_links();
		
		
		
		$data['user_list'] = $this->admin_model->get_all_spot($per_pg,$offset);

 $data['title']= 'Spot Conversion';
  $this->load->view('templates/header_view',$data);
		$this->load->view('admin/nav', $data);
		$this->load->view('admin/spot_blog', $data);
		$this->load->view('templates/footer_view',$data);
 }
 
  public function spot(){
	
$data['title']= 'Spot Conversion';	
 $id = $this->uri->segment(3);
        $data['admin'] = $this->admin_model->vcgetById($id);
       
	    $this->load->view('templates/header_view',$data);
        $this->load->view("admin/nav", $data);
        $this->load->view('admin/spotperson', $data);
		$this->load->view('templates/footer_view',$data);
      
 }
	
public function edit(){
	
	
 $id = $this->uri->segment(3);
        $data['admin'] = $this->admin_model->getById($id);
       
	    $this->load->view('templates/header_view',$data);
        $this->load->view("admin/nav", $data);
        $this->load->view('admin/personEdit', $data);
		$this->load->view('templates/footer_view',$data);
      
 }
	public function update()
	{

$mdata['timeofcall']=$_POST['timeofcall'];
$mdata['conversionagent']=$_POST['conversionagent'];
$mdata['upsold']=$_POST['upsold'];
$mdata['title']=$_POST['title'];
$mdata['firstname']=$_POST['firstname'];
$mdata['lastname']=$_POST['lastname'];
$mdata['phonenumber']=$_POST['phonenumber'];
$mdata['address1']=$_POST['address1'];
$mdata['address2']=$_POST['address2'];
$mdata['address3']=$_POST['address3'];
$mdata['postcode']=$_POST['postcode'];
$mdata['county']=$_POST['county'];
$mdata['town']=$_POST['town'];
$mdata['fstpaydate']=$_POST['fstpaydate'];
$mdata['c_acc']=$_POST['c_accno'];
$mdata['sc_exp']=$_POST['sc_expno'];
$mdata['bankname']=$_POST['bankname'];
$mdata['bankaccountname']=$_POST['bankaccountname'];
$mdata['boxtype']=$_POST['boxtype'];
$mdata['frequency']=$_POST['frequency'];
$mdata['amount']=$_POST['amount'];
$mdata['protype']=$_POST['product'];
$mdata['home_emergency']=$_POST['home_emergency'];
$mdata['password']=$_POST['password'];
$mdata['policynumber']=$_POST['policynumber'];
$mdata['finalpremium']=$_POST['finalpremium'];
$mdata['comments']=$_POST['comments'];
$mdata['centre']=$_POST['centre'];
$mdata['wgoods']=$_POST['wgoods'];
$mdata['status']=$_POST['status'];

$res=$this->admin_model->update_info($mdata, $_POST['id']);
if($res){

header('location:'.base_url()."index.php/admin/adminblog".$this->adminblog());

}
}

public function upd()
	{

$mdata['timeofcall']=$_POST['timeofcall'];
$mdata['conversionagent']=$_POST['conversionagent'];
$mdata['upsold']=$_POST['upsold'];
$mdata['title']=$_POST['title'];
$mdata['firstname']=$_POST['firstname'];
$mdata['lastname']=$_POST['lastname'];
$mdata['phonenumber']=$_POST['phonenumber'];
$mdata['address1']=$_POST['address1'];
$mdata['address2']=$_POST['address2'];
$mdata['address3']=$_POST['address3'];
$mdata['postcode']=$_POST['postcode'];
$mdata['county']=$_POST['county'];
$mdata['town']=$_POST['town'];
$mdata['fstpaydate']=$_POST['fstpaydate'];
$mdata['c_acc']=$_POST['c_accno'];
$mdata['sc_exp']=$_POST['sc_expno'];
$mdata['bankname']=$_POST['bankname'];
$mdata['bankaccountname']=$_POST['bankaccountname'];
$mdata['boxtype']=$_POST['boxtype'];
$mdata['frequency']=$_POST['frequency'];
$mdata['amount']=$_POST['amount'];
$mdata['protype']=$_POST['product'];
$mdata['home_emergency']=$_POST['home_emergency'];
$mdata['password']=$_POST['password'];
$mdata['policynumber']=$_POST['policynumber'];
$mdata['finalpremium']=$_POST['finalpremium'];
$mdata['comments']=$_POST['comments'];
$mdata['centre']=$_POST['centre'];
$mdata['wgoods']=$_POST['wgoods'];
$mdata['status']=$_POST['status'];

$res=$this->admin_model->upd_info($mdata, $_POST['id']);
if($res){

header('location:'.base_url()."index.php/admin/adminblog".$this->adminblog());

}
}
	
public function BoxDownload() {
		//load the download helper
		$this->load->helper('download');
		$this->excel->setActiveSheetIndex(0);
		$this->load->library('form_validation');
  // field name, error message, validation rules
  $this->form_validation->set_rules('fdate', 'From', 'trim|required|min_length[4]|xss_clean');
  $this->form_validation->set_rules('tdate', 'To', 'trim|required|min_length[4]|xss_clean');
  if($this->form_validation->run() == FALSE)
  {
   $this->boxreport();
  }
 
  else
  {
        // Gets all the data using MY_Model.php
		$saledate1=date('Y-m-d', strtotime($this->input->post('fdate')));
        $saledate2=date('Y-m-d', strtotime($this->input->post('tdate')));
		
		
		$fdate=$saledate1;
		$tdate=$saledate2;
        $data = $this->admin_model->get_all_excelbox($fdate,$tdate);

        $this->excel->stream('Report.xls', $data);
	}
	}
	// date_validation callback
	public function boxreport()
 {
 $data['title']= 'Reports';
  $this->load->view('templates/header_view',$data);
		$this->load->view('admin/nav', $data);
		$this->load->view('admin/boxreport', $data);
		$this->load->view('templates/footer_view',$data);
 }
 
 public function SpotDownload() {
		//load the download helper
		$this->load->helper('download');
		$this->excel->setActiveSheetIndex(0);
		$this->load->library('form_validation');
  // field name, error message, validation rules
  $this->form_validation->set_rules('fromdate', 'From', 'trim|required|min_length[4]|xss_clean');
  $this->form_validation->set_rules('todate', 'To', 'trim|required|min_length[4]|xss_clean');
  if($this->form_validation->run() == FALSE)
  {
   $this->spotreport();
  }
 
  else
  {
        // Gets all the data using MY_Model.php
		$vcsaledate1=date('Y-m-d', strtotime($this->input->post('fromdate')));
        $vcsaledate2=date('Y-m-d', strtotime($this->input->post('todate')));
		
		
		$fromdate=$vcsaledate1;
		$todate=$vcsaledate2;
        $data = $this->admin_model->get_all_excelspot($fromdate,$todate);

        $this->excel->stream('Report.xls', $data);
	}
	}
	// date_validation callback
	
	public function spotreport()
 {
 $data['title']= 'Reports';
  $this->load->view('templates/header_view',$data);
		$this->load->view('admin/nav', $data);
		$this->load->view('admin/spotreport', $data);
		$this->load->view('templates/footer_view',$data);
 }

public function logout()
	{
		$newdata = array(
		'user_id'   =>'',
		'user_name'  =>'',
		'user_email'     => '',
		'logged_in' => FALSE,
		);
		$this->session->unset_userdata($newdata );
		$this->session->sess_destroy();
		$this->index();
	}
}