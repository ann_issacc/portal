
<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title></title>

<script type="text/javascript">

function show_confirm(act,gotoid)

{

if(act=="editcall")



{

window.location="<?php echo base_url();?>index.php/vcagent/"+act+"/"+gotoid;

}

}

</script>

<script>

 function autoRefresh1()
{
	  window.location.reload();
}

setInterval('autoRefresh1()', 30000); //Refresh page every 30 seconds
</script>

</head>

<body>

<div class="row">
  <div class="col-md-9 col-md-push-2">
  <div class="jumbotron">
<div class="table">
<div class="container">
 <div class="table-responsive"> 
 <div class="paging"><?php echo $pagination; ?></div>
<div class="panel panel-default">
  <div class="panel-heading">
    <h2 class="panel-title"> <p class="style2">CALL BACK</p></h2>
  </div>
  
  <div class="panel-body">
        <table class="table table-hover">
 <thead>

                <tr>

<th >Id</th>

<th >Sale Date</th>

<th >Last Name</th>

<th >Phone Number</th>

<th>Conversion Agent</th>

<th >Time Of vc Call</th>

<th >Process Status</th>

<th >Status</th>



</tr>


            </thead>
<tbody>

            
<?php foreach ($user_list as $u_key){ ?>

           <tr >

<td><?php echo $u_key->id; ?></td>

<td><?php echo $u_key->saledate; ?></td>

<td><?php echo $u_key->lastname; ?></td>

<td><?php echo $u_key->phonenumber; ?></td>

<td><?php echo $u_key->conversionagent; ?></td>

<td><?php echo $u_key->timeofcall; ?></td>

<td>
<?php
/**
* If process is not set to empty display...
* In Use By:
* CurrentUsername
*
* Otherwise display...
* Available
*/
?>

<?php if ($u_key->process !== "") { ?>
In Use By:<br />
<?php echo $u_key->process; ?>
<?php } else { ?>
Available
<?php } ?>
</td>
<?php
/**
* If process is set to current user
* show yellow button and allow edit
* If process is not current user but is set
* show red DISABLED button with no link to not allow edit
* If process is not set
* show normal edit button
*/
?>

<td>
    <?php if ($u_key->process === $this->session->userdata('usname')) { ?>
        <a href="javascript:;" onClick="show_confirm('editcall', <?php
        echo $u_key->id;?>)" class="btn btn-warning btn-block"><strong>Call Back
        </strong></a>
    <?php } elseif ($u_key->process ==="") { ?>
        <a href="javascript:;" onClick="show_confirm('editcall', <?php
        echo $u_key->id;?>)" class="btn btn-primary btn-block"><strong>Call Back</strong></a>
    <?php } else { ?>
        <a href="javascript:;" class="btn btn-danger btn-block
        disabled"><strong>Call Back</strong></a>
    <?php } ?>
</td>
</tr>
<?php }?>
</tbody>

</table>
</div>
</div>
</div>
</div>
</div>
</div>
</div>

<div class="col-md-2 col-md-pull-9"><ul class="nav nav-pills nav-stacked" role="tablist">
  <li role="presentation" ><?php echo anchor('vcagent/vcblog', 'New Lead'); ?></li>
  <li role="presentation"><?php echo anchor('vcagent/callback', 'Call Backs'); ?></li>
   <li role="presentation"><?php echo anchor('vcagent/automated', 'Automated Response'); ?></li>
   <li role="presentation"><?php echo anchor('vcagent/sendletter', 'Send Letter'); ?></li>
  <li role="presentation"><?php echo anchor('vcagent/sale', 'Sale'); ?></li>
  <li role="presentation"><?php echo anchor('vcagent/upsold', 'Sale And upsold'); ?></li>
  <li role="presentation"><?php echo anchor('vcagent/declined_mislead', 'Declined(Mislead)'); ?></li>
   <li role="presentation"><?php echo anchor('vcagent/declined_notmislead', 'Declined(Not Mislead)'); ?></li>
  <li role="presentation"><?php echo anchor('vcagent/tcf', 'TCF-Vulnerable Prospect'); ?></li>
	<li role="presentation"><?php echo anchor('vcagent/dncblog', 'DNC'); ?></li>
</ul></div>
</div>
</div>
</div>
</body>

</html>
