<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Vcagent_model extends CI_Model {
 
  
 
    public function __construct()
    {
        parent::__construct();
    }
	function vclogin($username,$password)
 {
  $this->db->where("username",$username);
  $this->db->where("password",$password);

  $query=$this->db->get("vclogin");
  if($query->num_rows()>0)
  {
   foreach($query->result() as $rows)
   {
    //add all data to session
    $newdata = array(
      
      'usname'  => $rows->username,
	  
      
      'logged_in'  => TRUE,
    );
   }
   $this->session->set_userdata($newdata);
   // Set object to \DateTime object with time being "now"                            
    $now = new DateTime();
	                                                          
    // Create update data array with "last_login" set to "now" in "Y-m-d H:i:s" format 
    $data = array('last_login'=> $now->format('Y/m/d H:i:s'));                         
    // Update vclogin table with data for logged in username                           
    $this->db->update('vclogin', $data ,array('username'=> $username));      
   return true;
  }
  return false;
 }
 
	
	
	
	public function get_all_users($per_pg,$offset)

{
$this->db->order_by('id','desc');
$this->db->where('status', 'approved');
$this->db->or_where('status', 'new lead');
$query = $this->db->get('vc_customers',$per_pg,$offset);

return $query->result();

}

public function insert_users_to_db($data)

{

return $this->db->insert('vc_customers', $data);

}

public function getById($id){

// Get customer and check whether current owned by user
$currentUsername = $this->session->userdata('usname');
$query = $this->db->get_where('vc_customers',array('id'=>$id));
$customer = $query->row_array();
// If process is not empty and doesn�t equal curent username
if ($customer['process'] !== "" && $customer['process'] !== $currentUsername) {
// Return null if process is set and is not current username
return null;
}
// Update process to current username and return
$data = array('process'=> $currentUsername);
$this->db->update('vc_customers', $data ,array('id'=> $id));
$query = $this->db->get_where('vc_customers',array('id'=> $id));
return $query->row_array();
}

public function update_info($data,$id)

{

$this->db->where('vc_customers.id',$id);

return $this->db->update('vc_customers', $data);

}
public function get_sale_users($per_pg,$offset)

{
$this->db->order_by('id','desc');
$this->db->where('status', 'sale'); 
$query = $this->db->get('vc_customers',$per_pg,$offset);

return $query->result();

}
public function get_verified_sale($per_pg,$offset)

{
$this->db->order_by('id','desc');
$this->db->where('status', 'verified sale'); 
$this->db->or_where('status', 'Verified Upsold'); 
$query = $this->db->get('vc_customers',$per_pg,$offset);

return $query->result();

}
public function get_upsold_users($per_pg,$offset)

{
$this->db->order_by('id','desc');
$this->db->where('status', 'sale and upsold'); 
$query = $this->db->get('vc_customers',$per_pg,$offset);

return $query->result();

}

public function get_dnc_users($per_pg,$offset)

{
$this->db->order_by('id','desc');
$this->db->where('status', 'dnc'); 
$query = $this->db->get('vc_customers',$per_pg,$offset);

return $query->result();

}
public function get_notmislead_users($per_pg,$offset)

{
$this->db->order_by('id','desc');
$this->db->where('status', 'declined(not mislead)'); 
$query = $this->db->get('vc_customers',$per_pg,$offset);

return $query->result();

}
public function get_mislead_users($per_pg,$offset)

{
$this->db->order_by('id','desc');
$this->db->where('status', 'declined(mislead)'); 
$query = $this->db->get('vc_customers',$per_pg,$offset);

return $query->result();

}
public function get_automated_users($per_pg,$offset)

{
$this->db->order_by('id','desc');
$this->db->where('status', 'automated response'); 
$query = $this->db->get('vc_customers',$per_pg,$offset);

return $query->result();

}
public function get_letter_users($per_pg,$offset)

{
$this->db->order_by('id','desc');
$this->db->where('status', 'send letter'); 
$query = $this->db->get('vc_customers',$per_pg,$offset);

return $query->result();

}
public function get_tcf_users($per_pg,$offset)

{
$this->db->order_by('id','desc');
$this->db->where('status', 'tcf'); 
$query = $this->db->get('vc_customers',$per_pg,$offset);

return $query->result();

}
public function message_count()
	{
		return $this->db->count_all('agent_details');
	}
	public function get_call_users($per_pg,$offset)

{
$this->db->order_by('id','desc');
$this->db->where('status', 'call back'); 
$query = $this->db->get('vc_customers',$per_pg,$offset);

return $query->result();

}

	
}