<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Super extends CI_Controller{ 
	
	private $limit = 10;
	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->model('super_model','',TRUE);
		
		$this->load->library(array('table','form_validation'));
		
		// load helper
		$this->load->helper('url');
		
		// load model
		
	}
	public function index()
	{
		$data['title']= 'Index';
		$this->load->view('templates/header_view',$data);
		$this->load->view('super/super.php', $data);
		$this->load->view('templates/footer_view',$data);
	}



	
	public function superlogin()
 {
  $username=$this->input->post('username');
  $password=md5($this->input->post('pass'));

  $result=$this->super_model->superlogin($username,$password);
  if($result) $this->superblog();
  else        $this->index();
 }
	
  public function superblog()
 {
		$data['base']=$this->config->item('base_url');
		$data['title']= 'Sales Board ';
		$this->load->model("super_model");
		
		$total=$this->super_model->message_count();
		$per_pg=20;
		$offset=$this->uri->segment(3);
       			
		$this->load->library('pagination');
		$config['base_url'] = $data['base'].'/index.php/super/superblog/';
	    $config['total_rows'] = $total;
	    $config['per_page'] = $per_pg;
		$config['full_tag_open'] = '<div id="pagination">';
		$config['full_tag_close'] = '</div>';
            
        $this->pagination->initialize($config);
             
        $data['pagination']=$this->pagination->create_links();
		
		
		
		$data['user_list'] = $this->super_model->get_all_users($per_pg,$offset);


$this->load->view('templates/header_view',$data);
$this->load->view("super/nav", $data);
$this->load->view("super/super_blog", $data);
$this->load->view('templates/footer_view',$data);
	}
	
	
	
function update($id)
	{
		// set validation properties
		$this->_set_rules();
		
		// prefill form values
		$person = $this->super_model->get_by_id($id)->row();
		$this->form_data->id = $id;
		$this->form_data->name = $person->name;
		$this->form_data->phonenumber = $person->phonenumber;
		$this->form_data->address1 = $person->address1;
		$this->form_data->address2 = $person->address2;
		$this->form_data->address3 = $person->address3;
		$this->form_data->postcode = $person->postcode;
		$this->form_data->county = $person->county;
		$this->form_data->town = $person->town;
		$this->form_data->fstpaydate = $person->fstpaydate;
		$this->form_data->c_accno = $person->c_acc;
		$this->form_data->sc_expno = $person->sc_exp;
		$this->form_data->boxtype = $person->boxtype;
		$this->form_data->accountnumber = $person->acc_no;
		$this->form_data->frequency = $person->frequency;
		$this->form_data->amount = $person->amount;
		$this->form_data->product = $person->protype;
		$this->form_data->home_emergency = $person->home_emergency;
		$this->form_data->password = $person->password;
		$this->form_data->comments = $person->comments;
		$this->form_data->centre = $person->centre;
		$this->form_data->wgoods = $person->wgoods;
		$this->form_data->status = $person->status;
		
		
		// set common properties
		$data['title'] = 'Update person';
		$data['message'] = '';
		$data['action'] = site_url('admin/updatePerson');
		$data['link_back'] = anchor('admin/adminblog/','Back to list of persons',array('class'=>'back'));
	
		// load view
		$this->load->view('templates/header_view',$data);
		$this->load->view('super/nav', $data);
		$this->load->view('super/personEdit', $data);
		$this->load->view('templates/footer_view',$data);
	}
	
	function updatePerson()
	{
		// set common properties
		$data['title'] = 'Update person';
		$data['action'] = site_url('super/updatePerson');
		$data['link_back'] = anchor('super/index/','Back to list of persons',array('class'=>'back'));
		
		// set empty default form field values
		$this->_set_fields();
		// set validation properties
		$this->_set_rules();
		
		// run validation
		if ($this->form_validation->run() == FALSE)
		{
			$data['message'] = '';
		}
		else
		{
			// save data
			$id = $this->input->post('id');
			$person = array('name'=>$this->input->post('name'),
    'phonenumber'=>$this->input->post('phonenumber'),
    'address1'=>$this->input->post('address1'),
	'address2'=>$this->input->post('address2'),
	'address3'=>$this->input->post('address3'),
	'postcode'=>$this->input->post('postcode'),
	'county'=>$this->input->post('county'),
	'town'=>$this->input->post('town'),
	'fstpaydate'=>$this->input->post('fstpaydate'),
	'c_acc'=>$this->input->post('c_accno'),
	'sc_exp'=>$this->input->post('sc_expno'),
	'boxtype'=>$this->input->post('boxtype'),
	'acc_no'=>$this->input->post('accountnumber'),
	'frequency'=>$this->input->post('frequency'),
	'amount'=>$this->input->post('amount'),
	'protype'=>$this->input->post('product'),
	'home_emergency'=>$this->input->post('home_emergency'),
	'password'=>$this->input->post('password'),
	'comments'=>$this->input->post('comments'),
	'centre'=>$this->input->post('centre'),
	'wgoods'=>$this->input->post('wgoods'),
	'status'=>$this->input->post('status'),
	);
			$this->super_model->update($id,$person);
			
			// set user message
			$data['message'] = '<div class="success">update person success</div>';
		}
		
		// load view
		$this->load->view('templates/header_view',$data);
		$this->load->view('super/nav', $data);
		$this->load->view('super/personEdit', $data);
		
		$this->load->view('templates/footer_view',$data);
	}
	
	
	
	// set empty default form field values
	function _set_fields()
	{
		$this->form_data->id = '';
		$this->form_data->name = '';
		$this->form_data->phonenumber = '';
		$this->form_data->address1 = '';
		$this->form_data->address2 = '';
		$this->form_data->address3 = '';
		$this->form_data->postcode = '' ;
		$this->form_data->county = '';
		$this->form_data->town = '';
		$this->form_data->fstpaydate = '';
		$this->form_data->c_accno = '';
		$this->form_data->sc_expno = '';
		$this->form_data->boxtype = '';
		$this->form_data->accountnumber = '';
		$this->form_data->frequency = '';
		$this->form_data->amount = '';
		$this->form_data->product = '';
		$this->form_data->home_emergency = '';
		$this->form_data->password = '';
		$this->form_data->comments = '';
		$this->form_data->centre = '';
		$this->form_data->wgoods = '';
		$this->form_data->status = '';
	}
	
	// validation rules
	function _set_rules()
	{
		$this->form_validation->set_rules('name', 'Name', 'trim|required');
		$this->form_validation->set_rules('phonenumber', 'Phone Number', 'trim|required');
		$this->form_validation->set_rules('status', 'status', 'trim|required|callback_valid_date');
		
		$this->form_validation->set_message('required', '* required');
		$this->form_validation->set_message('isset', '* required');
		$this->form_validation->set_message('valid_date', 'date format is not valid. dd-mm-yyyy');
		$this->form_validation->set_error_delimiters('<p class="error">', '</p>');
	}
	
	// date_validation callback
	// date_validation callback
	public function report()
 {
 $data['title']= 'Reports';
  $this->load->view('templates/header_view',$data);
		$this->load->view('super/nav', $data);
		$this->load->view('super/report', $data);
		$this->load->view('templates/footer_view',$data);
 }
 public function register()
 {
 $data['agentdetails'] = $this->super_model->add_user();
  $data['title']= 'Register';
  $this->load->view('templates/header_view',$data);
  $this->load->view('super/nav.php', $data);
  $this->load->view('super/register.php', $data);
  $this->load->view('templates/footer_view',$data);
 }

public function registration()
 {
  $this->load->library('form_validation');
  // field name, error message, validation rules
  $this->form_validation->set_rules('user_name', 'User Name', 'trim|required|min_length[4]|xss_clean');
  $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[4]|max_length[32]');
  $this->form_validation->set_rules('con_password', 'Password Confirmation', 'trim|required|matches[password]');

  if($this->form_validation->run() == FALSE)
  {
   $this->index();
  }
  else
  {
   $this->super_model->add_user();
   $this->thank();
  }
 }
public function logout()
	{
		$newdata = array(
		'user_id'   =>'',
		'user_name'  =>'',
		'user_email'     => '',
		'logged_in' => FALSE,
		);
		$this->session->unset_userdata($newdata );
		$this->session->sess_destroy();
		$this->index();
	}
}