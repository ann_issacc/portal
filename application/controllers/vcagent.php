<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Vcagent extends CI_Controller{ 
	public $limit = 10;
	
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Europe/London');
		
		$this->load->model('vcagent_model','',TRUE);
		
		$this->load->library(array('table','form_validation','session'));
		$this->load->helper(array('form', 'url'));
		// load helper
		$this->load->helper('url');
		
		// load model
		
	}
	public function index()
	{
		$data['title']= 'Index';
		$this->load->view('templates/header_view',$data);
		$this->load->view('templates/index.php', $data);
		$this->load->view('templates/footer_view',$data);
	}


public function vcagentindex()
	{
		if(($this->session->userdata('usname')!=""))
		{
			$this->vcblog();
		}
		else{
			$data['title']= 'Home';
			$this->load->view('templates/header_view',$data);
			$this->load->view("vcagent/vc_agent.php", $data);
			$this->load->view('templates/footer_view',$data);
		}
	}
	
	public function vclogin()
 {
 $this->load->library('form_validation');
 $this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
$this->form_validation->set_rules('pass', 'Password', 'trim|required|xss_clean');

if ($this->form_validation->run() == FALSE) {

			$data['title']= 'Home';
			$this->load->view('templates/header_view',$data);
			$this->load->view("vcagent/vc_agent.php", $data);
			$this->load->view('templates/footer_view',$data);
			} 
			else 
			{
  $username=$this->input->post('username');
  $password=md5($this->input->post('pass'));

  $result=$this->vcagent_model->vclogin($username,$password);

  if($result) { $this->vcblog(); 
  }
  else   { $msg['error_message'] =  'Invalid Username or Password';

             $this->load->view('templates/header_view');
			$this->load->view("vcagent/vc_agent.php", $msg);
			$this->load->view('templates/footer_view');
 }
 }
 }
	
 public function vcblog()
 {
		$data['base']=$this->config->item('base_url');
		$data['title']= 'Sales Board ';
		$this->load->model("vcagent_model");
		
		$total=$this->vcagent_model->message_count();
		$per_pg=20;
		$offset=$this->uri->segment(3);
       			
		$this->load->library('pagination');
		$config['base_url'] = $data['base'].'/index.php/vcagent/vcblog/';
	    $config['total_rows'] = $total;
	    $config['per_page'] = $per_pg;
		$config['full_tag_open'] = '<div id="pagination">';
		$config['full_tag_close'] = '</div>';
            
        $this->pagination->initialize($config);
             
        $data['pagination']=$this->pagination->create_links();
		
		
		
		$data['user_list'] = $this->vcagent_model->get_all_users($per_pg,$offset);


$this->load->view('templates/header_view',$data);
$this->load->view("vcagent/nav", $data);
$this->load->view("vcagent/vc_blog", $data);
$this->load->view('templates/footer_view',$data);
	}
	
	
public function edit(){
	
	
 $id = $this->uri->segment(3);
// Set $data[�vcagent�] to model
// if model return null (see above) redirect to vcblog listing
if (null === $data['vcagent'] = $this->vcagent_model->getById($id)) {
redirect('/vcagent/vcblog', 'refresh');
return;
}
// Output page
$this->load->view('templates/header_view',$data);
$this->load->view("vcagent/nav", $data);
$this->load->view('vcagent/personEdit', $data);
$this->load->view('templates/footer_view',$data);
}
 
 public function editcall(){
	
	
 $id = $this->uri->segment(3);
// Set $data[�vcagent�] to model
// if model return null (see above) redirect to vcblog listing
if (null === $data['vcagent'] = $this->vcagent_model->getById($id)) {
redirect('/vcagent/callback', 'refresh');
return;
}
       
	    $this->load->view('templates/header_view',$data);
        $this->load->view("vcagent/nav", $data);
        $this->load->view('vcagent/editcallback', $data);
		$this->load->view('templates/footer_view',$data);
      
 }
	public function update()
	{
$mdata['timeofcall']=$_POST['timeofcall'];
$mdata['conversionagent']=$_POST['conversionagent'];
$mdata['upsold']=$_POST['upsold'];
$mdata['title']=$_POST['title'];
$mdata['firstname']=$_POST['firstname'];
$mdata['lastname']=$_POST['lastname'];
$mdata['phonenumber']=$_POST['phonenumber'];
$mdata['address1']=$_POST['address1'];
$mdata['address2']=$_POST['address2'];
$mdata['address3']=$_POST['address3'];
$mdata['postcode']=$_POST['postcode'];
$mdata['county']=$_POST['county'];
$mdata['town']=$_POST['town'];
$mdata['fstpaydate']=$_POST['fstpaydate'];
$mdata['c_acc']=$_POST['c_accno'];
$mdata['sc_exp']=$_POST['sc_expno'];
$mdata['bankname']=$_POST['bankname'];
$mdata['bankaccountname']=$_POST['bankaccountname'];
$mdata['boxtype']=$_POST['boxtype'];
$mdata['frequency']=$_POST['frequency'];
$mdata['initialamount']=$_POST['initialamount'];
$mdata['typeoftv']=$_POST['typeoftv'];
$mdata['home_emergency']=$_POST['home_emergency'];
$mdata['password']=$_POST['password'];
$mdata['policynumber']=$_POST['policynumber'];
$mdata['finalpremium']=$_POST['finalpremium'];
$mdata['comments']=$_POST['comments'];
$mdata['centre']=$_POST['centre'];
$mdata['vccomments']=$_POST['vccomments'];
$mdata['autorescomments']=$_POST['autorescomments'];
$mdata['wgoods']=$_POST['wgoods'];
$mdata['status']=$_POST['status'];
$mdata['process']=$_POST['process'];

$res=$this->vcagent_model->update_info($mdata, $_POST['id']);
if($res){

header('location:'.base_url()."index.php/vcagent/vcblog".$this->vcblog());

}
}
	

	
	// date_validation callback
	public function callback()
 {
 
		$data['base']=$this->config->item('base_url');
		$data['title']= 'Sales Board ';
		$this->load->model("vcagent_model");
		
		$total=$this->vcagent_model->message_count();
		$per_pg=20;
		$offset=$this->uri->segment(3);
       			
		$this->load->library('pagination');
		$config['base_url'] = $data['base'].'/index.php/vcagent/callback/';
	    $config['total_rows'] = $total;
	    $config['per_page'] = $per_pg;
		$config['full_tag_open'] = '<div id="pagination">';
		$config['full_tag_close'] = '</div>';
            
        $this->pagination->initialize($config);
             
        $data['pagination']=$this->pagination->create_links();
		
		
		
		$data['user_list'] = $this->vcagent_model->get_call_users($per_pg,$offset);

 $data['title']= 'Call Back';
  $this->load->view('templates/header_view',$data);
		$this->load->view('vcagent/nav', $data);
		$this->load->view('vcagent/call_list', $data);
		$this->load->view('templates/footer_view',$data);
 }
 public function sale()
 {
 
		$data['base']=$this->config->item('base_url');
		$data['title']= 'Sales Board ';
		$this->load->model("vcagent_model");
		
		$total=$this->vcagent_model->message_count();
		$per_pg=20;
		$offset=$this->uri->segment(3);
       			
		$this->load->library('pagination');
		$config['base_url'] = $data['base'].'/index.php/vcagent/sale/';
	    $config['total_rows'] = $total;
	    $config['per_page'] = $per_pg;
		$config['full_tag_open'] = '<div id="pagination">';
		$config['full_tag_close'] = '</div>';
            
        $this->pagination->initialize($config);
             
        $data['pagination']=$this->pagination->create_links();
		
		
		
		$data['user_list'] = $this->vcagent_model->get_sale_users($per_pg,$offset);

 $data['title']= 'Sale';
  $this->load->view('templates/header_view',$data);
		$this->load->view('vcagent/nav', $data);
		$this->load->view('vcagent/sale', $data);
		$this->load->view('templates/footer_view',$data);
 }
 
 public function sales(){
	
$data['title']= 'Sale';	
 $id = $this->uri->segment(3);
        $data['vcagent'] = $this->vcagent_model->getById($id);
       
	    $this->load->view('templates/header_view',$data);
        $this->load->view("vcagent/nav", $data);
        $this->load->view('vcagent/sales', $data);
		$this->load->view('templates/footer_view',$data);
      
 }
public function upsold()
 {
 
		$data['base']=$this->config->item('base_url');
		$data['title']= 'Sales Board ';
		$this->load->model("vcagent_model");
		
		$total=$this->vcagent_model->message_count();
		$per_pg=20;
		$offset=$this->uri->segment(3);
       			
		$this->load->library('pagination');
		$config['base_url'] = $data['base'].'/index.php/vcagent/upsold/';
	    $config['total_rows'] = $total;
	    $config['per_page'] = $per_pg;
		$config['full_tag_open'] = '<div id="pagination">';
		$config['full_tag_close'] = '</div>';
            
        $this->pagination->initialize($config);
             
        $data['pagination']=$this->pagination->create_links();
		
		
		
		$data['user_list'] = $this->vcagent_model->get_upsold_users($per_pg,$offset);

 $data['title']= 'Upsold';
  $this->load->view('templates/header_view',$data);
		$this->load->view('vcagent/nav', $data);
		$this->load->view('vcagent/upsold', $data);
		$this->load->view('templates/footer_view',$data);
 }
  
  public function upsolds(){
	
$data['title']= 'Sale And Upsold';	
 $id = $this->uri->segment(3);
        $data['vcagent'] = $this->vcagent_model->getById($id);
       
	    $this->load->view('templates/header_view',$data);
        $this->load->view("vcagent/nav", $data);
        $this->load->view('vcagent/upsolds', $data);
		$this->load->view('templates/footer_view',$data);
      
 }
public function declined_mislead()
 {
 
		$data['base']=$this->config->item('base_url');
		$data['title']= 'Sales Board ';
		$this->load->model("vcagent_model");
		
		$total=$this->vcagent_model->message_count();
		$per_pg=20;
		$offset=$this->uri->segment(3);
       			
		$this->load->library('pagination');
		$config['base_url'] = $data['base'].'/index.php/vcagent/declined_mislead/';
	    $config['total_rows'] = $total;
	    $config['per_page'] = $per_pg;
		$config['full_tag_open'] = '<div id="pagination">';
		$config['full_tag_close'] = '</div>';
            
        $this->pagination->initialize($config);
             
        $data['pagination']=$this->pagination->create_links();
		
		
		
		$data['user_list'] = $this->vcagent_model->get_mislead_users($per_pg,$offset);

 $data['title']= 'Mislead';
  $this->load->view('templates/header_view',$data);
		$this->load->view('vcagent/nav', $data);
		$this->load->view('vcagent/declined_mislead', $data);
		$this->load->view('templates/footer_view',$data);
 }
 
 public function misleads(){
	
$data['title']= 'Mis-Lead';	
 $id = $this->uri->segment(3);
        $data['vcagent'] = $this->vcagent_model->getById($id);
       
	    $this->load->view('templates/header_view',$data);
        $this->load->view("vcagent/nav", $data);
        $this->load->view('vcagent/misleads', $data);
		$this->load->view('templates/footer_view',$data);
      
 }
 
 public function dncblog()
 {
 
		$data['base']=$this->config->item('base_url');
		$data['title']= 'Sales Board ';
		$this->load->model("vcagent_model");
		
		$total=$this->vcagent_model->message_count();
		$per_pg=20;
		$offset=$this->uri->segment(3);
       			
		$this->load->library('pagination');
		$config['base_url'] = $data['base'].'/index.php/vcagent/dncblog/';
	    $config['total_rows'] = $total;
	    $config['per_page'] = $per_pg;
		$config['full_tag_open'] = '<div id="pagination">';
		$config['full_tag_close'] = '</div>';
            
        $this->pagination->initialize($config);
             
        $data['pagination']=$this->pagination->create_links();
		
		
		
		$data['user_list'] = $this->vcagent_model->get_dnc_users($per_pg,$offset);

 $data['title']= 'DNC';
  $this->load->view('templates/header_view',$data);
		$this->load->view('vcagent/nav', $data);
		$this->load->view('vcagent/dncblog', $data);
		$this->load->view('templates/footer_view',$data);
 }
 
 
 public function dnc(){
	
$data['title']= 'DNC';	
 $id = $this->uri->segment(3);
        $data['vcagent'] = $this->vcagent_model->getById($id);
       
	    $this->load->view('templates/header_view',$data);
        $this->load->view("vcagent/nav", $data);
        $this->load->view('vcagent/dnc', $data);
		$this->load->view('templates/footer_view',$data);
      
 }
 public function declined_notmislead()
 {
 
		$data['base']=$this->config->item('base_url');
		$data['title']= 'Sales Board ';
		$this->load->model("vcagent_model");
		
		$total=$this->vcagent_model->message_count();
		$per_pg=20;
		$offset=$this->uri->segment(3);
       			
		$this->load->library('pagination');
		$config['base_url'] = $data['base'].'/index.php/vcagent/declined_notmislead/';
	    $config['total_rows'] = $total;
	    $config['per_page'] = $per_pg;
		$config['full_tag_open'] = '<div id="pagination">';
		$config['full_tag_close'] = '</div>';
            
        $this->pagination->initialize($config);
             
        $data['pagination']=$this->pagination->create_links();
		
		
		
		$data['user_list'] = $this->vcagent_model->get_notmislead_users($per_pg,$offset);

 $data['title']= 'Not Mislead';
  $this->load->view('templates/header_view',$data);
		$this->load->view('vcagent/nav', $data);
		$this->load->view('vcagent/declined_notmislead', $data);
		$this->load->view('templates/footer_view',$data);
 }
 
 public function notmisleads(){
	
$data['title']= 'Sale And Upsold';	
 $id = $this->uri->segment(3);
        $data['vcagent'] = $this->vcagent_model->getById($id);
       
	    $this->load->view('templates/header_view',$data);
        $this->load->view("vcagent/nav", $data);
        $this->load->view('vcagent/notmisleads', $data);
		$this->load->view('templates/footer_view',$data);
      
 }
 public function automated()
 {
 
		$data['base']=$this->config->item('base_url');
		$data['title']= 'Sales Board ';
		$this->load->model("vcagent_model");
		
		$total=$this->vcagent_model->message_count();
		$per_pg=20;
		$offset=$this->uri->segment(3);
       			
		$this->load->library('pagination');
		$config['base_url'] = $data['base'].'/index.php/vcagent/automated/';
	    $config['total_rows'] = $total;
	    $config['per_page'] = $per_pg;
		$config['full_tag_open'] = '<div id="pagination">';
		$config['full_tag_close'] = '</div>';
            
        $this->pagination->initialize($config);
             
        $data['pagination']=$this->pagination->create_links();
		
		
		
		$data['user_list'] = $this->vcagent_model->get_automated_users($per_pg,$offset);

 $data['title']= 'Automated';
  $this->load->view('templates/header_view',$data);
		$this->load->view('vcagent/nav', $data);
		$this->load->view('vcagent/automated', $data);
		$this->load->view('templates/footer_view',$data);
 }
 public function auto(){
	
$data['title']= 'Sale And Upsold';	
  $id = $this->uri->segment(3);
// Set $data[�vcagent�] to model
// if model return null (see above) redirect to vcblog listing
if (null === $data['vcagent'] = $this->vcagent_model->getById($id)) {
redirect('/vcagent/automated', 'refresh');
return;
}
       
	    $this->load->view('templates/header_view',$data);
        $this->load->view("vcagent/nav", $data);
        $this->load->view('vcagent/auto', $data);
		$this->load->view('templates/footer_view',$data);
      
 }
 
  public function verified()
 {
 
		$data['base']=$this->config->item('base_url');
		$data['title']= 'Sales Board ';
		$this->load->model("vcagent_model");
		
		$total=$this->vcagent_model->message_count();
		$per_pg=20;
		$offset=$this->uri->segment(3);
       			
		$this->load->library('pagination');
		$config['base_url'] = $data['base'].'/index.php/vcagent/verified/';
	    $config['total_rows'] = $total;
	    $config['per_page'] = $per_pg;
		$config['full_tag_open'] = '<div id="pagination">';
		$config['full_tag_close'] = '</div>';
            
        $this->pagination->initialize($config);
             
        $data['pagination']=$this->pagination->create_links();
		
		
		
		$data['user_list'] = $this->vcagent_model->get_verified_sale($per_pg,$offset);

 $data['title']= 'Verified';
  $this->load->view('templates/header_view',$data);
		$this->load->view('vcagent/nav', $data);
		$this->load->view('vcagent/verified', $data);
		$this->load->view('templates/footer_view',$data);
 }
 public function verifiedsale(){
	
$data['title']= 'Verified Sale';	
 $id = $this->uri->segment(3);
        $data['vcagent'] = $this->vcagent_model->getById($id);
       
	    $this->load->view('templates/header_view',$data);
        $this->load->view("vcagent/nav", $data);
        $this->load->view('vcagent/verifiedsale', $data);
		$this->load->view('templates/footer_view',$data);
      
 }
 public function sendletter()
 {
 
		$data['base']=$this->config->item('base_url');
		$data['title']= 'Sales Board ';
		$this->load->model("vcagent_model");
		
		$total=$this->vcagent_model->message_count();
		$per_pg=20;
		$offset=$this->uri->segment(3);
       			
		$this->load->library('pagination');
		$config['base_url'] = $data['base'].'/index.php/vcagent/sendletter/';
	    $config['total_rows'] = $total;
	    $config['per_page'] = $per_pg;
		$config['full_tag_open'] = '<div id="pagination">';
		$config['full_tag_close'] = '</div>';
            
        $this->pagination->initialize($config);
             
        $data['pagination']=$this->pagination->create_links();
		
		
		
		$data['user_list'] = $this->vcagent_model->get_letter_users($per_pg,$offset);

 $data['title']= 'Send Letter';
  $this->load->view('templates/header_view',$data);
		$this->load->view('vcagent/nav', $data);
		$this->load->view('vcagent/sendletter', $data);
		$this->load->view('templates/footer_view',$data);
 }
 
 public function letter(){
	
$data['title']= 'Sale And Upsold';	
 $id = $this->uri->segment(3);
// Set $data[�vcagent�] to model
// if model return null (see above) redirect to vcblog listing
if (null === $data['vcagent'] = $this->vcagent_model->getById($id)) {
redirect('/vcagent/vcblog', 'refresh');
return;
}
       
	    $this->load->view('templates/header_view',$data);
        $this->load->view("vcagent/nav", $data);
        $this->load->view('vcagent/letter', $data);
		$this->load->view('templates/footer_view',$data);
      
 }
 public function tcf()
 {
 
		$data['base']=$this->config->item('base_url');
		$data['title']= 'Sales Board ';
		$this->load->model("vcagent_model");
		
		$total=$this->vcagent_model->message_count();
		$per_pg=20;
		$offset=$this->uri->segment(3);
       			
		$this->load->library('pagination');
		$config['base_url'] = $data['base'].'/index.php/vcagent/tcf/';
	    $config['total_rows'] = $total;
	    $config['per_page'] = $per_pg;
		$config['full_tag_open'] = '<div id="pagination">';
		$config['full_tag_close'] = '</div>';
            
        $this->pagination->initialize($config);
             
        $data['pagination']=$this->pagination->create_links();
		
		
		
		$data['user_list'] = $this->vcagent_model->get_tcf_users($per_pg,$offset);

 $data['title']= 'TCF';
  $this->load->view('templates/header_view',$data);
		$this->load->view('vcagent/nav', $data);
		$this->load->view('vcagent/tcf', $data);
		$this->load->view('templates/footer_view',$data);
 }
 
 public function tcfs(){
	
$data['title']= 'Sale And Upsold';	
 $id = $this->uri->segment(3);
        $data['vcagent'] = $this->vcagent_model->getById($id);
       
	    $this->load->view('templates/header_view',$data);
        $this->load->view("vcagent/nav", $data);
        $this->load->view('vcagent/tcfs', $data);
		$this->load->view('templates/footer_view',$data);
      
 }
public function logout()
	{
		$newdata = array(
		'username'   =>'',
		'password'  =>'',
		
		'logged_in' => FALSE,
		);
		$this->session->unset_userdata($newdata );
		$this->session->sess_destroy();
		$this->index();
	}
}