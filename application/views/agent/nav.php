<style type="text/css">
<!--
.style1 {color: #0099FF}
-->
</style>
<nav class="navbar navbar-default" role="navigation">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">      <a class="navbar-brand">Signed in as agent <span class="style1"><?php echo $this->session->userdata('uname'); ?>!</span></a> </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="active"></li>
        <li></li>
        <li class="dropdown">
         
          <ul class="dropdown-menu" role="menu">
            
            <li class="divider"></li>
            <li></li>
            <li class="divider"></li>
            <li></li>
          </ul>
        </li>
      </ul>
      
      <ul class="nav navbar-nav navbar-right">
        <li><h5><strong><?php echo anchor('agent/logout', 'Logout'); ?></strong></h5>
        </li>
       
            
      </ul>
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>

